clear all
close all


% load rain frames
% The programme works on 5 consectutive frames to derain the central frame.
fileContents= dir(['rainFrames/*jpg']);
for nf=1:length(fileContents)
    rainFrames(:,:,:,nf)= im2uint8(imread(['rainFrames/',fileContents(nf).name]));
end


paramsIn.useGPU= true; % false
paramsIn.numSP= 288; % recommended 288 for VGA resolution images. adjust proptionately for different resolutions.

% Input: rainFrames: mxnxNF
% Output: derainFrames mxnxNF (note the first and last two frames are left as empty) 
derainFrames= derainFunction(rainFrames, paramsIn);


