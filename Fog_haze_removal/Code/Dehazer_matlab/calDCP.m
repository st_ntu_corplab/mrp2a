function [DCP]= calDCP(Img,patchSize)

[wid, len, cc]= size(Img);
iterSegDim= 500;

if(cc==3)
    
    DCP= zeros(wid, len);

    ImgEx= padarray(Img,[patchSize-1,patchSize-1],1,'post');
    numSegs= ceil(wid/iterSegDim)*ceil(len/iterSegDim);

    for ii=1:numSegs
        [indxR,indxC]= ind2sub([ceil(wid/iterSegDim),ceil(len/iterSegDim)],ii); 
        stRow= (indxR-1)*iterSegDim+1;
        stCol= (indxC-1)*iterSegDim+1;
        enRow= min(stRow+iterSegDim-1,wid);
        enCol= min(stCol+iterSegDim-1,len);

        blkMatrix= im2col(ImgEx(stRow:enRow+patchSize-1,stCol:enCol+patchSize-1,1),[patchSize,patchSize],'sliding');
        blkMin1= min(blkMatrix);

        blkMatrix= im2col(ImgEx(stRow:enRow+patchSize-1,stCol:enCol+patchSize-1,2),[patchSize,patchSize],'sliding');
        blkMin2= min(blkMatrix);
        blkMin1= min(blkMin1,blkMin2);    

        blkMatrix= im2col(ImgEx(stRow:enRow+patchSize-1,stCol:enCol+patchSize-1,3),[patchSize,patchSize],'sliding');
        blkMin2= min(blkMatrix);
        blkMin= min(blkMin1,blkMin2);

        DCP(stRow:min(stRow+iterSegDim-1,wid),stCol:min(stCol+iterSegDim-1,len))...
                = reshape(blkMin,enRow-stRow+1,enCol-stCol+1);
    end
    clear blkMatrix blkMin blkMin1 blkMin2;

%     [DCP, IntVar] = guidedfilter_color(Img, DCP, 60, 10^-6);

elseif(cc==1)
    
    DCP= zeros(wid, len);

    ImgEx= padarray(Img,[patchSize-1,patchSize-1],1,'post');
    iterSegDim= 500;
    numSegs= ceil(wid/iterSegDim)*ceil(len/iterSegDim);

    for ii=1:numSegs
        [indxR,indxC]= ind2sub([ceil(wid/iterSegDim),ceil(len/iterSegDim)],ii); 
        stRow= (indxR-1)*iterSegDim+1;
        stCol= (indxC-1)*iterSegDim+1;
        enRow= min(stRow+iterSegDim-1,wid);
        enCol= min(stCol+iterSegDim-1,len);

        blkMatrix= im2col(ImgEx(stRow:enRow+patchSize-1,stCol:enCol+patchSize-1),[patchSize,patchSize],'sliding');
        blkMin= min(blkMatrix);      

        DCP(stRow:min(stRow+iterSegDim-1,wid),stCol:min(stCol+iterSegDim-1,len))...
                = reshape(blkMin,enRow-stRow+1,enCol-stCol+1);
    end
    clear blkMatrix blkMin;

%     [DCP, IntVar] = guidedfilter(Img, DCP, 60, 10^-6);
    
end

end
