
clear;

fileFolderPath= ['\\eee-mtl-wdclp\Share folder\_STEng\HazeData\datasetFattal\'];
% fileFolderPath= ['\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Haze\datasetFattal\Noisy\'];

list = dir([fileFolderPath,'/*', '.mat']);
contentNames = setdiff({list.name}, {'.', '..'});
numContents = length(contentNames);

% diffRes= 0:0.05:0.5;
% dpRes= 0:0.1:1;
% hs= zeros(length(diffRes),length(dpRes)); 
% hsdc= zeros(length(diffRes),length(dpRes));
% for n=3:numContents
for n=3;
breakIdx= strfind(contentNames{n},'_');
fileName= contentNames{n}(1:(breakIdx(1)-1));
load([fileFolderPath,contentNames{n}]);    
[nRow,nCol,nCC]= size(I);
% skyMask= im2double(imread([fileFolderPath,'skyMask\',fileName,'_mask.png']));
skyMask= im2double(imread([fileFolderPath,'.\skyMask\',fileName,'_mask.png']));
cIdx= find(skyMask);

% imwrite(I,[fileFolderPath,'hazy\',fileName,'.png'],'png');
% imwrite(Jtrue,[fileFolderPath,'hazy\',fileName,'_Jtrue.png'],'png');
% imwrite(Ttrue,[fileFolderPath,'hazy\',fileName,'_Ttrue.png'],'png');
% Ai(n,:)=squeeze(A); 
% end
% imwrite(I,[num2str(n),'.',fileName,'.png'],'png');
% end
% IG= rgb2gray(I);
% figure;imshow(double(skyMask).*abs(T-Ttrue));
% figure;imshow(double(skyMask).*abs(Tdc-Ttrue));

%% calculate DCP
% darkChan= calDCP(I,7); %darkChan
% TdarkChan= 1- min(darkChan/(min(A)),0.95);

TdarkChan= Tdc;
% figure;imshow(TdarkChan);
% TdarkChan= guidedfilter(rgb2gray(I),TdarkChan,60,10^-6); %darkChan

%% estimate airlight
% A = reshape(estimate_airlight(im2double(imresize(I,1)).^(1)),1,1,3);
% A= EstimateAirLight(darkChan,I);
% figure;imshow(repmat(A,100,100,1));
% figure;imshow(repmat(Ai,100,100,1));

% darkChan= calDCP(I,7);
% TdarkChan= 1- darkChan/(min(A));
% figure;imshow(TdarkChan);

Jdc= dehazing(I,A,TdarkChan);
% figure;imshow(Jdc);

NL = non_local_dehazing(I, A);
% figure;pcshow(NL.clusterVec,'MarkerSize',500);
rdc= sqrt(sum((Jdc - repmat(A,nRow,nCol,1)).^2,3));

reffedClusterIdx= unique(NL.clusterIdxMap)';
clusterRadiusMaxDc= zeros(nRow,nCol);
clusterRadiusMaxNew= zeros(nRow,nCol);
changeMask= zeros(nRow,nCol);
for nc=reffedClusterIdx
    currIdx= find(NL.clusterIdxMap==nc);
    rVecMaxDc= rdc(currIdx);
    rVecMaxNL= NL.clusterRadiusMax(currIdx);
    
    currHazelineMax= median(rVecMaxNL);
	clusterRadiusMaxNew(currIdx)= min(1,min(median(rVecMaxNL),median(rVecMaxDc)));
    if(median(rVecMaxNL)>median(rVecMaxDc))
        changeMask(currIdx)=1;
    end
    
end
% figure;imshow(clusterRadiusMaxNew);
% figure;imshow(NL.clusterRadiusMax);
%{
[Axx{n},Vxx{n}]= distribution2d(TmaxGT(1,:),TmaxGT(2,:),1,[0:0.01:1],[0:0.01:1]);
h=imagesc(Vxx{n},[0,1]);xlabel('R_max'); ylabel('Saturation');
colorbar;
greenColorMap = [linspace(0, 1, 128),linspace(1, 0, 128)];
redColorMap = [linspace(0, 1, 128), ones(1, 128)];
blueColorMap = [ones(1, 132), linspace(1, 0, 124)];
colorMap = [redColorMap; greenColorMap; blueColorMap]';
colormap(colorMap);
%}

% [~,Is,~]= rgb2hsv(I);

clusterRadiusMaxNew= medfilt2(clusterRadiusMaxNew,[5,5]);
 
Tnl= NL.clusterRadius./NL.clusterRadiusMax;
TNLReg= RegularizationTrans(Tnl,I,NL.weight); % figure;imshow(TNLReg);
Jnl= dehazing(I,A,TNLReg); % figure;imshow(Jnl);
% Tnew= min(min(rdc,NL.clusterRadius)./clusterRadiusMaxNew,0.95);
Tnew= min(NL.clusterRadius./clusterRadiusMaxNew,0.95);

% Apply lower bound from the image (Eqs. (13-14))
trans_lower_bound = 1 - min(bsxfun(@rdivide,I,reshape(A,1,1,3)) ,[],3);
Tnew_t = max(Tnew, trans_lower_bound);
TReg= RegularizationTrans(Tnew_t,I,NL.weight);
% TReg= RegularizationTrans(Tnew,I,NL.weight);

Jnew= dehazing(I,A,TReg); % figure;imshow(Jnew);
Jdiff= abs(mean((Jnew- Jnl),3));

disp(sprintf('%s:\tDCP: %.3f\tcolorline: %.3f\tNL: %.3f/%.3f\tNew: %.3f/%.3f/%.3f',fileName,...
mean(abs(Tdc(cIdx)-Ttrue(cIdx))),...
mean(abs(T(cIdx) -Ttrue(cIdx))),...
mean(abs(Tnl(cIdx) -Ttrue(cIdx))),...
mean(abs(TNLReg(cIdx) -Ttrue(cIdx))),...
mean(abs(Tnew_t(cIdx)-Ttrue(cIdx))),...
mean(abs(TReg(cIdx) -Ttrue(cIdx)))));
% mean(Jdiff(cIdx))));
% mean(abs(Tcomp(cIdx) -Ttrue(cIdx))),...
% mean(abs(TcompReg(cIdx) -Ttrue(cIdx)))));

% if(~exist([fileFolderPath,list(n).name(1:end-4),'_output']))
%     mkdir([fileFolderPath,list(n).name(1:end-4),'_output']);
% end
% imwrite(Jnew,[fileFolderPath,list(n).name(1:end-4),'_output\','J_mine.png'],'png');
% imwrite(Jnl,[fileFolderPath,list(n).name(1:end-4),'_output\','Jnl.png'],'png');
% imwrite(TNLReg,[fileFolderPath,list(n).name(1:end-4),'_output\','Tnl.png'],'png');
% imwrite(TReg,[fileFolderPath,list(n).name(1:end-4),'_output\','T_mine.png'],'png');
% imwrite(Ttrue,[fileFolderPath,list(n).name(1:end-4),'_output\','T_GT.png'],'png');
% imwrite(Tdc,[fileFolderPath,list(n).name(1:end-4),'_output\','T_DC.png'],'png');
Jdiff;
        
end

