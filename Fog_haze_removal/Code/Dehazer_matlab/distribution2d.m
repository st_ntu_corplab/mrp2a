function [Ax,Vx]= distribution2d(X,Y,Z,xVec,yVec)

% quantization
resX= xVec(2)-xVec(1);
rangeX= length(xVec);
Xq= ceil(X/resX);
Xq(Xq<1)=1;
Xq(Xq>rangeX)=rangeX;


resY= yVec(2)-yVec(1);
rangeY= length(yVec);
Yq= ceil(Y/resY);
Yq(Yq<1)=1;
Yq(Yq>rangeY)=rangeY;
% 
% A= zeros(rangeX,rangeY);
% for xi=1:length(xVec)
%     for yi=1:length(yVec)
%         currIdx= find((Xq==xi).*(Yq==yi));
%         if(length(currIdx)~=0)
%             A(xi,yi)= mean(Z(currIdx));
%         end
%     end
% end

validX= unique(Xq(:));
range(validX);
validY= unique(Yq(:));
range(validY);

Vx=accumarray([Xq(:),Yq(:)],Z(:),[rangeX,rangeY],@(x) sum(x));


Ax=accumarray([Xq(:),Yq(:)],Z(:),[rangeX,rangeY],@(x) mean(x));


end