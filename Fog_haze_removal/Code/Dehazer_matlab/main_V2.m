clear;

IN.dataSet= 0; % 0 for the fattal dataset with ground truth
               % 1 for other images without ground true
               % 2 for image sequences from video 
               % 3 for direct video sequence
IN.writeOutput= 0; % 1 for enabling writing output results

switch IN.dataSet
    case 1
%         IN.fileFolderPath= ['\\eee-mtl-wdclp\Share folder\_STEng\HazeData\20160427 LiHe\'];
%         IN.list = dir([IN.fileFolderPath,'\','*.png']);
        IN.fileFolderPath= ['C:\Users\Jake\Desktop\derain\'];
        IN.list = dir([IN.fileFolderPath,'\','*.jpg']);
        IN.contentNames = setdiff({IN.list.name}, {'.', '..'});
        IN.numContents = length(IN.contentNames);
    case 0
        IN.fileFolderPath= ['\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Haze\datasetFattal\Scattering\'];
        IN.list = dir([IN.fileFolderPath,'/*', '.mat']);
        IN.contentNames = setdiff({IN.list.name}, {'.', '..'});
        IN.numContents = length(IN.contentNames);
    case 2
        IN.fileFolderPath= ['\\eee-mtl-wdclp\Share folder\_STEng\HazeData\20170511 LiHe\haze_beijing_frames\'];
        IN.list = dir([IN.fileFolderPath,'\','*.png']);
        IN.contentNames = setdiff({IN.list.name}, {'.', '..'});
        IN.numContents = length(IN.contentNames);
        IN.ABufferSize = 25;
        IN.ABuffer= zeros(3, IN.ABufferSize);
        % IN.weight= [0.4,0.3,0.1,0.1,0.1];
        IN.weight= ones(1,IN.ABufferSize)/IN.ABufferSize;
    case 3
        fileFolder = '\\eee-mtl-wdclp\Share folder\_STEng\HazeData\20170511 LiHe\';
        fileName= 'haze_singapore.mp4';

        hvfrInput = vision.VideoFileReader([fileFolder,fileName],'ImageColorSpace',...
                                    'RGB','VideoOutputDataType', 'double');                                 
        S = info(hvfrInput);
        fSize = S.VideoSize;                          
       
        hvfw = vision.VideoFileWriter([fileFolder,fileName(1:end-4),'_dehazed.avi'],'AudioInputPort',...
                                    false,'FrameRate', S.VideoFrameRate);
        % S = info(hvfrRain);
        % fSize = S.VideoSize;
        % hcsc = vision.ColorSpaceConverter('Conversion', 'RGB to intensity');
        IN.numContents= 4500; % 4500 for 3min; 3000 for 2 min; 1500 for 1 min;
        IN.ABufferSize = 25;
        IN.ABuffer= zeros(3, IN.ABufferSize);
        % IN.weight= [0.4,0.3,0.1,0.1,0.1];
        IN.weight= ones(1,IN.ABufferSize)/IN.ABufferSize;
end


for n=1:IN.numContents
% for n=4;

str = sprintf('processing frame %d', n);
disp(str);
    
if(IN.dataSet==0)
    fileName= IN.contentNames{n}(1:end-16);
    load([IN.fileFolderPath,IN.contentNames{n}]);    
    [nRow,nCol,nCC]= size(I);
    skyMask= im2double(imread([IN.fileFolderPath,'skyMask\',fileName(1:end-3),'_mask.png']));
    cIdx= find(skyMask);
elseif(IN.dataSet==1 ||IN.dataSet==2)
    fileName= IN.contentNames{n}(1:end-4);
    I= im2double(imread([IN.fileFolderPath,IN.contentNames{n}]));    
    [nRow,nCol,nCC]= size(I);
elseif(IN.dataSet== 3)
    if(isDone(hvfrInput))
        break;
    end
    I= im2double(step(hvfrInput)); % figure;imshow(I);
    [nRow,nCol,nCC]= size(I);
    ratio= (640/nCol);
    I= imresize(I,ratio,'bicubic');
    [nRow,nCol,nCC]= size(I);
end

%% calculate DCP
darkChan= calDCP(I,7); %darkChan
% figure;imshow(darkChan);

%% estimate airlight
if(IN.dataSet==1 ||IN.dataSet==2 || IN.dataSet==3)
[A, locA] = EstimateAirLight(darkChan,I);
% figure;imshow(repmat(A,100,100,1));
% figure;imshow(insertMarker(I,locA,'circle','size',5,'color','r'));
    if(IN.dataSet==2 || IN.dataSet==3)
        IN.ABuffer(:,2:end)= IN.ABuffer(:,1:(end-1));
        IN.ABuffer(:,1)= A;         
        A= sum(IN.ABuffer.*repmat(IN.weight,3,1),2)/sum((IN.ABuffer(2,:)~=0).*IN.weight,2);
        A= reshape(A,1,1,3);
        % figure;imshow(repmat(A,100,100,1));
    end
end

TdarkChan=1- min(darkChan/(min(A)),0.95);
% figure;imshow(TdarkChan);
% TdarkChan= guidedfilter(rgb2gray(I),TdarkChan,60,10^-6); %darkChan
Jdc= dehazing(I,A,TdarkChan);
% figure;imshow(Jdc);

NL = NLRadius(I, A);
% figure;pcshow(NL.clusterVec,'MarkerSize',500);
rdc= sqrt(sum((Jdc - repmat(A,nRow,nCol,1)).^2,3));

reffedClusterIdx= unique(NL.clusterIdxMap)';
clusterRadiusMaxDc= zeros(nRow,nCol);
clusterRadiusMaxNew= zeros(nRow,nCol);
changeMask= zeros(nRow,nCol);

for nc=reffedClusterIdx
    currIdx= find(NL.clusterIdxMap==nc);
    rVecMaxDc= rdc(currIdx);
    rVecMaxNL= NL.clusterRadiusMax(currIdx);
    
    currHazelineMax= median(rVecMaxNL);
	clusterRadiusMaxNew(currIdx)= min(1,min(median(rVecMaxNL),median(rVecMaxDc)));
    if(median(rVecMaxNL)>median(rVecMaxDc))
        changeMask(currIdx)=1;
    end
    
end

Tnl= min(NL.clusterRadius./NL.clusterRadiusMax,0.95);
Tnew= min(min(rdc,NL.clusterRadius)./clusterRadiusMaxNew,0.95);

% Apply lower bound from the image (Eqs. (13-14))
trans_lower_bound = 1 - min(bsxfun(@rdivide,I,reshape(A,1,1,3)) ,[],3);
Tnew_t = max(Tnew, trans_lower_bound);
TReg= RegularizationTrans(Tnew_t,I,NL.weight); % figure;imshow(TReg);

if(IN.dataSet==0)
    disp(sprintf('%s:\tDCP: %.3f\tcolorline: %.3f\tNL: %.3f/...\tNew: %.3f/%.3f',fileName,...
    mean(abs(Tdc(cIdx)-Ttrue(cIdx))),...
    mean(abs(T(cIdx) -Ttrue(cIdx))),...
    mean(abs(Tnl(cIdx) -Ttrue(cIdx))),...
    mean(abs(Tnew(cIdx)-Ttrue(cIdx))),...
    mean(abs(TReg(cIdx) -Ttrue(cIdx)))));
end
% mean(abs(Tcomp(cIdx) -Ttrue(cIdx))),...
% mean(abs(TcompReg(cIdx) -Ttrue(cIdx)))));

Tnl_t = max(Tnl, trans_lower_bound);
TNLReg= RegularizationTrans(Tnl_t,I,NL.weight); % figure;imshow(TNLReg);
Jnl= dehazing(I,A,TNLReg); % figure;imshow(Jnl);

J= dehazing(I,A,TReg); % figure;imshow(TReg);
% figure; imshow([insertMarker(I,locA,'circle','size',5,'color','r'),J]);
% imwrite([I,Jnl,J],sprintf('D:\\hazeData\\%.4d.jpg',n),'jpg');

if(IN.writeOutput==1)
    if(IN.dataSet==0)
        if(~exist([IN.fileFolderPath,IN.list(n).name(1:end-4),'_output']))
            mkdir([IN.fileFolderPath,IN.list(n).name(1:end-4),'_output']);
        end
        imwrite(J,[IN.fileFolderPath,IN.list(n).name(1:end-4),'_output\','J_mine.png'],'png');
        imwrite(Jnl,[IN.fileFolderPath,IN.list(n).name(1:end-4),'_output\','Jnl.png'],'png');
        imwrite(TNLReg,[IN.fileFolderPath,IN.list(n).name(1:end-4),'_output\','Tnl.png'],'png');
        imwrite(TReg,[IN.fileFolderPath,IN.list(n).name(1:end-4),'_output\','T_mine.png'],'png');
	if(IN.dataSet==1 || IN.dataSet==2 ) 
        if(~exist([IN.fileFolderPath,'output']))
            mkdir([IN.fileFolderPath,'output']);
        end
        imwrite(J,[IN.fileFolderPath,'output\',fileName,'_dehazed.png'],'png');
        %imwrite(TReg,[IN.fileFolderPath,'output\',fileName,'_tmap.png'],'png');
    elseif(IN.dataSet==3)  
        Jnl= dehazing(I,A,Tnl);
      fwrite= [I,J];       
        step(hvfw, fwrite);
    end
            
end

end
end

if(IN.dataSet==3)        
    release(hvfrInput);
    release(hvfw);       
end


