function Xq= quantizeMap(X,xVec)

resX= xVec(2)-xVec(1);
rangeX= length(xVec);
Xq= ceil(X/resX);
Xq(Xq<1)=1;
Xq(Xq>rangeX)=rangeX;

end