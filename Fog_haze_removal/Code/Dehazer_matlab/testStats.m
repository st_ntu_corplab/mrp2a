% clear;

fileFolderPath= ['\\eee-mtl-wdclp\Share folder\_STEng\HazeData\datasetFattal\'];
list = dir([fileFolderPath,'/*', '.mat']);
contentNames = setdiff({list.name}, {'.', '..'});
numContents = length(contentNames);

n=3;
breakIdx= strfind(contentNames{n},'_');
fileName= contentNames{n}(1:(breakIdx(1)-1));
load([fileFolderPath,contentNames{n}]);    
[nRow,nCol,nCC]= size(I);
skyMask= im2double(imread([fileFolderPath,'.\skyMask\',fileName,'_mask.png']));
cIdx= find(skyMask);

Igt = imread('D:\Dropbox\Work\Rain_removal\Database\haze\GT\flower1_Jtrue.png');
Igt = double(Igt)/255;

NLgt = non_local_dehazing(Igt, A);
NL = non_local_dehazing(I, A);

[h,w,~] = size(Igt);
diffClass = NLgt.clusterIdxMap == NL.clusterIdxMap;
fracCorrectClass = sum(diffClass(:))/ (h*w)

nCluster = 1000;
arrRadius = cell(nCluster,1);
arrClusterIdx = cell(nCluster,1);

for i=1:nCluster
    idx_pix = find(NLgt.clusterIdxMap == i);
    arrRadius{i} = NLgt.clusterRadius(idx_pix);
    arrClusterIdx{i} = idx_pix;
end

arrI = reshape(I,[],3);
arrRGB = arrI(arrClusterIdx{9},:) - [0.5 0.6 1];

figure; scatter3(arrRGB(:,1), arrRGB(:,2), arrRGB(:,3));
xlabel('x');ylabel('y');zlabel('z');
hold on;
scatter3(0,0,0,3);

mid = [329 279];
patch = I(mid(1)-10:mid(1)+10,mid(2)-10:mid(2)+10,1:3);
patch = reshape(patch,[],3) - [0.5 0.6 1];
figure; scatter3(patch(:,1), patch(:,2), patch(:,3));
xlabel('x');ylabel('y');zlabel('z');
hold on;
scatter3(0,0,0,3);