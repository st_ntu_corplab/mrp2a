
clear;

fileFolderPath= ['\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Haze\datasetFattal\Noisy\'];
% fileFolderPath= ['\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Haze\datasetFattal\Noisy\'];

list = dir([fileFolderPath,'/*', '.mat']);
contentNames = setdiff({list.name}, {'.', '..'});
numContents = length(contentNames);

% diffRes= 0:0.05:0.5;
% dpRes= 0:0.1:1;
% hs= zeros(length(diffRes),length(dpRes)); 
% hsdc= zeros(length(diffRes),length(dpRes));
for n=1:numContents
% for n=1;
breakIdx= strfind(contentNames{n},'_');
fileName= contentNames{n}(1:(breakIdx(1)-1));
load([fileFolderPath,contentNames{n}]);    
[nRow,nCol,nCC]= size(I);
% skyMask= im2double(imread([fileFolderPath,'skyMask\',fileName,'_mask.png']));
skyMask= im2double(imread([fileFolderPath,'..\skyMask\',fileName,'_mask.png']));
cIdx= find(skyMask);

% imwrite(I,[fileFolderPath,'hazy\',fileName,'.png'],'png');
% imwrite(Jtrue,[fileFolderPath,'hazy\',fileName,'_Jtrue.png'],'png');
% imwrite(Ttrue,[fileFolderPath,'hazy\',fileName,'_Ttrue.png'],'png');
% Ai(n,:)=squeeze(A); 
% end
% imwrite(I,[num2str(n),'.',fileName,'.png'],'png');
% end
% IG= rgb2gray(I);
% figure;imshow(double(skyMask).*abs(T-Ttrue));
% figure;imshow(double(skyMask).*abs(Tdc-Ttrue));

%% calculate DCP
% darkChan= calDCP(I,7); %darkChan
% TdarkChan= 1- min(darkChan/(min(A)),0.95);

TdarkChan= Tdc;
% figure;imshow(TdarkChan);
% TdarkChan= guidedfilter(rgb2gray(I),TdarkChan,60,10^-6); %darkChan

%% estimate airlight
% A = reshape(estimate_airlight(im2double(imresize(I,1)).^(1)),1,1,3);
% A= EstimateAirLight(darkChan,I);
% figure;imshow(repmat(A,100,100,1));
% figure;imshow(repmat(Ai,100,100,1));

% darkChan= calDCP(I,7);
% TdarkChan= 1- darkChan/(min(A));
% figure;imshow(TdarkChan);

Jdc= dehazing(I,A,TdarkChan);
% figure;imshow(Jdc);

NL = non_local_dehazing(I, A);
% figure;pcshow(NL.clusterVec,'MarkerSize',500);
rdc= sqrt(sum((Jdc - repmat(A,nRow,nCol,1)).^2,3));

reffedClusterIdx= unique(NL.clusterIdxMap)';
clusterRadiusMaxDc= zeros(nRow,nCol);
clusterRadiusMaxNew= zeros(nRow,nCol);
changeMask= zeros(nRow,nCol);
for nc=reffedClusterIdx
    currIdx= find(NL.clusterIdxMap==nc);
    rVecMaxDc= rdc(currIdx);
    rVecMaxNL= NL.clusterRadiusMax(currIdx);
    
    currHazelineMax= median(rVecMaxNL);
	clusterRadiusMaxNew(currIdx)= min(1,min(median(rVecMaxNL),median(rVecMaxDc)));
    if(median(rVecMaxNL)>median(rVecMaxDc))
        changeMask(currIdx)=1;
    end
    
end
% figure;imshow(clusterRadiusMaxNew);
% figure;imshow(NL.clusterRadiusMax);
%{
[Axx{n},Vxx{n}]= distribution2d(TmaxGT(1,:),TmaxGT(2,:),1,[0:0.01:1],[0:0.01:1]);
h=imagesc(Vxx{n},[0,1]);xlabel('R_max'); ylabel('Saturation');
colorbar;
greenColorMap = [linspace(0, 1, 128),linspace(1, 0, 128)];
redColorMap = [linspace(0, 1, 128), ones(1, 128)];
blueColorMap = [ones(1, 132), linspace(1, 0, 124)];
colorMap = [redColorMap; greenColorMap; blueColorMap]';
colormap(colorMap);
%}

% [~,Is,~]= rgb2hsv(I);

clusterRadiusMaxNew= medfilt2(clusterRadiusMaxNew,[5,5]);
 
Tnl= NL.clusterRadius./NL.clusterRadiusMax;
TNLReg= RegularizationTrans(Tnl,I,NL.weight); % figure;imshow(TNLReg);
Jnl= dehazing(I,A,TNLReg); % figure;imshow(Jnl);
% Tnew= min(min(rdc,NL.clusterRadius)./clusterRadiusMaxNew,0.95);
Tnew= min(NL.clusterRadius./clusterRadiusMaxNew,0.95);

% Apply lower bound from the image (Eqs. (13-14))
trans_lower_bound = 1 - min(bsxfun(@rdivide,I,reshape(A,1,1,3)) ,[],3);
Tnew_t = max(Tnew, trans_lower_bound);
TReg= RegularizationTrans(Tnew_t,I,NL.weight);
% TReg= RegularizationTrans(Tnew,I,NL.weight);

Jnew= dehazing(I,A,TReg); % figure;imshow(Jnew);
Jdiff= abs(mean((Jnew- Jtrue),3));

disp(sprintf('%s:\tDCP: %.3f\tcolorline: %.3f\tNL: %.3f/%.3f\tNew: %.3f/%.3f/%.3f',fileName,...
mean(abs(Tdc(cIdx)-Ttrue(cIdx))),...
mean(abs(T(cIdx) -Ttrue(cIdx))),...
mean(abs(Tnl(cIdx) -Ttrue(cIdx))),...
mean(abs(TNLReg(cIdx) -Ttrue(cIdx))),...
mean(abs(Tnew_t(cIdx)-Ttrue(cIdx))),...
mean(abs(TReg(cIdx) -Ttrue(cIdx))),...
mean(Jdiff(cIdx))));
% mean(abs(Tcomp(cIdx) -Ttrue(cIdx))),...
% mean(abs(TcompReg(cIdx) -Ttrue(cIdx)))));

if(~exist([fileFolderPath,list(n).name(1:end-4),'_output']))
    mkdir([fileFolderPath,list(n).name(1:end-4),'_output']);
end
imwrite(Jnew,[fileFolderPath,list(n).name(1:end-4),'_output\','J_mine.png'],'png');
imwrite(Jnl,[fileFolderPath,list(n).name(1:end-4),'_output\','Jnl.png'],'png');
imwrite(TNLReg,[fileFolderPath,list(n).name(1:end-4),'_output\','Tnl.png'],'png');
imwrite(TReg,[fileFolderPath,list(n).name(1:end-4),'_output\','T_mine.png'],'png');
imwrite(Ttrue,[fileFolderPath,list(n).name(1:end-4),'_output\','T_GT.png'],'png');
imwrite(Tdc,[fileFolderPath,list(n).name(1:end-4),'_output\','T_DC.png'],'png');

        
end


cl= calDCP(clusterRadiusMaxNew,11);



Tq= NL.clusterRadius./clg;
Tqg= calDCP(Tq,11);
Tqgr= guidedfilter(clg,Tqg,30,10^-6); % darkChan


Tqg= medfilt2(Tq,[5,5]);


Tqgr= guidedfilter(rgb2gray(I),Tqg,30,10^-6); % darkChan
figure;imshow(Tqgr)
mean(abs(TqReg(cIdx)-Ttrue(cIdx)))

Tq= min(Tq,1);
mean(abs(Tq(cIdx)-Ttrue(cIdx)))

rMaxq= guidedfilter(rgb2gray(I),NL.clusterRadiusMax,60,10^-6); %darkChan
rMaxq2= guidedfilter(Tdc,clusterRadiusMaxNew,60,10^-6); %darkChan
Tnew2= min(NL.clusterRadius./rMaxq2,0.95);



Tq= Tdc.*(1-NL.weight)+Tnew.*NL.weight;
TqG= guidedfilter(rgb2gray(I),Tq,60,10^-6); %darkChan
mean(abs(Tq(cIdx)-Ttrue(cIdx)))


TqReg= RegularizationTrans(Tq,I,NL.weight);
mean(abs(TqReg(cIdx)-Ttrue(cIdx)))

Isj= (I-repmat(A,nRow,nCol,1));
Isj= Isj+ repmat(abs(min(Isj(:))),nRow,nCol,3);
[h,s,v]= rgb2hsv(Isj);




radiusBorder= 0.2;
iSkyMask= ((Tnew- TdarkChan)>radiusBorder).*(NL.clusterRadius<radiusBorder);
iWhiteMask= ((Tnew- Tdc)>radiusBorder).*(NL.clusterRadius>=radiusBorder);
% figure;imshow(iSkyMask);
% figure;imshow(iWhiteMask);
Tnew_t= Tnew.*(~iSkyMask)+ iSkyMask.*min(Tnew,Tdc);
TReg_t= RegularizationTrans(Tnew_t,I,min(~iWhiteMask,NL.weight));
mean(abs(TReg_t(cIdx) -Ttrue(cIdx)))




% Set bilateral filter parameters.
w     = 5;       % bilateral filter half-width
sigma = [3 0.1]; % bilateral filter standard deviations
TqReg = bfilter2(Tq,w,sigma);
mean(abs(TqReg(cIdx) -Ttrue(cIdx)))
% [Axx{n},Vxx{n}]= distribution2d(Tnew,Is,(Tnew-Ttrue),[0:0.01:1],[0:0.01:1]);
% figure;h=imagesc(Vxx{n},[-100,100]);xlabel('Saturation'); ylabel('Transmission');
% colorbar;
% greenColorMap = [linspace(0, 1, 128),linspace(1, 0, 128)];
% redColorMap = [linspace(0, 1, 128), ones(1, 128)];
% blueColorMap = [ones(1, 132), linspace(1, 0, 124)];
% colorMap = [redColorMap; greenColorMap; blueColorMap]';
% colormap(colorMap);
% % saveas(gcf,sprintf('summedError_%d.png',n));
% 
% 
% [Axx{n},Vxx{n}]= distribution2d(Tnl,Is,(Tnl-Tnew),[0:0.01:1],[0:0.01:1]);
% figure;h=imagesc(Vxx{n},[-10,10]);xlabel('Saturation'); ylabel('Transmission');
% colorbar;
% greenColorMap = [linspace(0, 1, 128),linspace(1, 0, 128)];
% redColorMap = [linspace(0, 1, 128), ones(1, 128)];
% blueColorMap = [ones(1, 132), linspace(1, 0, 124)];
% colorMap = [redColorMap; greenColorMap; blueColorMap]';
% colormap(colorMap);
% % saveas(gcf,sprintf('summedChange_%d.png',n));


% figure;imagesc((Tnl- Tnew),[-1,1]);
% colorbar;
% greenColorMap = [linspace(0, 1, 128),linspace(1,0,128)];
% redColorMap = [linspace(0, 1, 128), ones(1, 128)];
% blueColorMap = [ones(1, 128), linspace(1, 0, 128)];
% colorMap = [redColorMap; greenColorMap; blueColorMap]';
% colormap(colorMap);

% centers = {(0:0.01:1),(0:0.01:1)};
% hist3([TReg(cIdx),Is(cIdx)],'Ctrs',centers);
% xlabel('TReg'); ylabel('Sat');
% 
% K_std = accumarray(Is,TReg);
% 
% figure;imshow(Is<0.2)
% figure;imshow(Tnew>0.8)

%{
h= hist3([Tnew(:),Is(:)],'Edges',{(0:0.01:1),(0:0.01:1)});
figure;imagesc(h,[0,256]);xlabel('Transmission'); ylabel('Saturation');
colorbar;
greenColorMap = [linspace(1, 0, 256)];
redColorMap = [ones(1, 256)];
blueColorMap = [linspace(1, 0, 256)];
colorMap = [redColorMap; greenColorMap; blueColorMap]';
colormap(colorMap);
% saveas(gcf,sprintf('ST_%d.png',n));
%}

% Jnew= dehazing(I,A,Tnew);
% [~,Isnew,~]= rgb2hsv(Jnew);

%{
h= hist3([Tnl(:),Is(:)],'Edges',{(0:0.01:1),(0:0.010:1)});
figure;imagesc(h,[0,1]);xlabel('Saturation'); ylabel('Transmission');
colorbar;
greenColorMap = [linspace(1, 0, 256)];
redColorMap = [ones(1, 256)];
blueColorMap = [linspace(1, 0, 256)];
colorMap = [redColorMap; greenColorMap; blueColorMap]';
colormap(colorMap);
% saveas(gcf,sprintf('ST_%d.png',n));
%}

[Xt,Yt]= ind2sub([size(Axx,1),size(Axx,2)],find(cell2mat(Axx)));
K = convhull(Xt,Yt);


% end






%{
for n=1:numContents

% Display - will use some weird color map to start with.
h=imagesc(Ax{n},[-0.5,0.5]);xlabel('Transmission'); ylabel('Saturation');
colorbar;
greenColorMap = [linspace(0, 1, 128),linspace(1, 0, 128)];
redColorMap = [linspace(0, 1, 128), ones(1, 128)];
blueColorMap = [ones(1, 132), linspace(1, 0, 124)];
colorMap = [redColorMap; greenColorMap; blueColorMap]';
colormap(colorMap);
saveas(gcf,sprintf('avg_%d.png',n));

imagesc(Vx{n},[-0.5,0.5]);xlabel('Transmission'); ylabel('Saturation');
colorbar;
greenColorMap = [linspace(0, 1, 128),linspace(1, 0, 128)];
redColorMap = [linspace(0, 1, 128), ones(1, 128)];
blueColorMap = [ones(1, 132), linspace(1, 0, 124)];
colorMap = [redColorMap; greenColorMap; blueColorMap]';
colormap(colorMap);
saveas(gcf,sprintf('var_%d.png',n));

end
%}

% 
% bar3(ht)
% xlabel('TReg'); ylabel('True');
% colormap(hot)
% title('Seamount:Data Point Density Histogram and Intensity Map');
% grid on
% view(3);

% darkChan= calDCP(Jdc,7);
% darkChan= guidedfilter(rgb2gray(I),darkChan,60,10^-6);
% TdarkChan= 1- darkChan/(min(A));

% figure;imshow(Ttrue)
% figure;imshow(Tnew)
% figure;imshow(Tnl)
% figure;imshow(TReg-Ttrue)

% 
% % NL.weight;
% 
% errorNew= abs(Tnew-Ttrue);
% errorNL= abs(Tnl-Ttrue);
% 
% 
% figure;imshow((errorNew>errorNL).*skyMask);
% 
% 
% 
% 
% 
% figure;imshow(abs(testing-Ttrue).*skyMask);
% figure;imshow((testing<Ttrue).*skyMask);
% figure;imshow((Tnew<Ttrue).*skyMask);
% figure;imshow(repmat((testing>Tnew).*skyMask,1,1,3).*I);
% 
% figure;imshow((NL.clusterRadiusMax>clusterRadiusMaxDc).*skyMask);
% 
% 
% figure;imshow(NL.clusterRadius./rdc);
% figure;imshow(T<Ttrue);
% 
% 
% figure;imshow(dehazing(I,A,Tdc));
% 
% 
% darkChanTrue= min(A)*(1-Ttrue);
% % figure;imshow(darkChanTrue);
% % figure;imshow(Ttrue);
% 
% 
% 
% % figure;imshow(darkChanNL);
% % figure;imshow(1- darkChanNL/(max(A)));
% % figure;imshow(Ttrue);
% 
% figure;imshow(darkChan.*darkChanNL);
% 
% figure;imshow(abs(darkChan-darkChanNL));
% 
% x= rgb2hsv(I);


% nonLocal= double(skyMask).*(abs(T-Ttrue)<abs(Tdc-Ttrue));
% Local= double(skyMask).*(abs(T-Ttrue)>=abs(Tdc-Ttrue));
% figure;imshow(repmat(nonLocal,1,1,3).*I);
% figure;imshow(repmat(Local,1,1,3).*I);

% ImVar = mean(stdfilt(I,ones(7)),3);
%{
figure;
row=3;col=3;
subplot(row,col,1);imshow(I);
subplot(row,col,2);imshow(repmat(Local,1,1,3).*I);
subplot(row,col,3);imshow(repmat(nonLocal,1,1,3).*I);
subplot(row,col,4);imshow(Ttrue);
subplot(row,col,5);imshow(Tdc);
subplot(row,col,6);imshow(T);
subplot(row,col,8);imshow(skyMask.*(abs(Tdc-Ttrue))>0.1);
subplot(row,col,9);imshow(skyMask.*(abs(T-Ttrue))>0.1);

% subplot(row,col,7);imshow(skyMask.*(IG>0.5));
% subplot(row,col,8);imshow(skyMask.*ImVar>0.02);
% subplot(row,col,9);imshow(T);
%}


% h= hist3([abs(T(cIdx)-Ttrue(cIdx)),Ttrue(cIdx)],{diffRes dpRes});
% hdc= hist3([abs(Tdc(cIdx)-Ttrue(cIdx)),Ttrue(cIdx)],{diffRes dpRes});
% 
% hs= hs+ h;
% hsdc= hsdc+ hdc;


% disp([fileName,'  Fatal: ',num2str(mean(abs(Tfat(cIdx)-Ttrue(cIdx)))),...
%         '  DCP: ',num2str(mean(abs(Tdc(cIdx)-Ttrue(cIdx)))),...
%         '  nonLocal: ',num2str(mean(abs(T(cIdx)-Ttrue(cIdx))))]);
% % end
% 
% Th= T.*(abs(T-Ttrue)<abs(Tdc-Ttrue))+ Tdc.*(abs(T-Ttrue)>=abs(Tdc-Ttrue));
% disp([fileName,'  Fatal: ',num2str(mean(abs(Th(cIdx)-Ttrue(cIdx))))]);
% 
% 
% locIdx= find(Local);
% nlocIdx= find(nonLocal);
% v = mean(stdfilt(I,ones(7)),3);
% figure;hist(v(locIdx))
% figure;hist(v(nlocIdx))
% 
% % classification based on intensity
% hist([])



% figure;bar3(hs);
% xlabel('error'); ylabel('transmission');
% figure;bar3(hsdc);
% xlabel('error'); ylabel('transmission');