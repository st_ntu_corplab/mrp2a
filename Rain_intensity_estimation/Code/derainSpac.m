clear;
warning off;

addpath(genpath('required/'));

% Read data files
contentFolder= '..\Database\';
vList= dir([contentFolder,'\MIX*']);
for nn=1:length(vList)
    fprintf([num2str(nn),'. ',vList(nn).name,'\n']);
end
vNo= input('Choose Video Sequence: ');
fileFolder= [contentFolder,vList(vNo).name,'\'];
pngList= dir([fileFolder,'\*.png']);   
numFrames= length(pngList);    
[mRow,mCol,nCC]= size(imread([fileFolder,'/',pngList(1).name]));

outputFolder= ['..\results\' vList(vNo).name '\derainSPAC\'];
mkdir([outputFolder,'rainMasks/']);

% Store data for 5 consecutive frames in buffers. Processed frame is middle frame.
sizeBuffer= 5;
rainBuffer= zeros(mRow,mCol,nCC,sizeBuffer); % frame
rainBufferY= zeros(mRow,mCol,sizeBuffer); % frame (grayscale)
rainIntensityRec= zeros(1,sizeBuffer); % groundtruth rain intensity value
nCount= 0; % processed frame number

% Superpixel (SP) matching pareamters
spInfo.NP= 288; % Number of SPs
spMatchParams.blkSize= 80; 
spMatchParams.search_y= 30; % search window is larger than template in y dimension by 2*spMatchParams.search_y pixels
spMatchParams.search_x= 30; % search window is larger than template in x dimension by 2*spMatchParams.search_x pixels

% Run program
% partially fill buffer
for ni= 1:(sizeBuffer-1)
    newFrame= im2double(imread([fileFolder,'/',pngList(ni).name]));
    rainBuffer(:,:,:,ni+1)= rgb2ycbcr(newFrame);
    rainBufferY(:,:,ni+1)= squeeze(rainBuffer(:,:,1,ni+1));
    
    % update rain intensity record in buffer
    strLoc= strfind(pngList(ni).name,'_');
    newInten= str2num(pngList(ni).name(strLoc(end)+1:end-5));
    rainIntensityRec(ni+1)= newInten;
end

numFrames = 149;
for ni=sizeBuffer:numFrames
    
    %  1. input new frame and manage buffer update
    newFrame= im2double(imread([fileFolder,'/',pngList(ni).name]));
    
    % update frame in buffer
    rainBuffer(:,:,:,1:end-1)= rainBuffer(:,:,:,2:end);
    rainBuffer(:,:,:,end)= rgb2ycbcr(newFrame);
    rainBufferY(:,:,1:end-1)= rainBufferY(:,:,2:end);
    rainBufferY(:,:,end)= squeeze(rainBuffer(:,:,1,end));
    
    currFrame= rainBuffer(:,:,:,(sizeBuffer+1)/2); % currently processed frame
    currFrameY= rainBufferY(:,:,(sizeBuffer+1)/2);
    
    % update rain intensity in buffer
    strLoc= strfind(pngList(ni).name,'_');
    newInten= str2num(pngList(ni).name(strLoc(end)+1:end-5));
    rainIntensityRec(1:end-1)= rainIntensityRec(2:end);
    rainIntensityRec(end)= newInten;
    currInten= rainIntensityRec((sizeBuffer+1)/2);
    
    % 2. SP segmentation for current frame
    procFrame = ycbcr2rgb(currFrame);
    [spInfo.spIm, spInfo.numSP]= slicmex(im2uint8(procFrame),spInfo.NP,30); % apply SP segmentation
    
    referenceIdx= [1,2,4,5,3]; 
    bufferIm= im2uint8(rainBufferY(:,:,referenceIdx(1:sizeBuffer-1))); % only neighbor frames in bufferIm
    targetIm= im2uint8(currFrameY); 
   
    % Apply SP matching
    spMatchParams.useSpMask = 1;
    spMatchParams.useInputMask = 0;
    spMatchParams.f_splabel= int32(spInfo.spIm);
    spMatchParams.f_mask= uint8(ones(mRow,mCol));    
    currMatch = spMatching_mine(targetIm, bufferIm, spMatchParams); % apply SP matching
    
    rainMask= zeros(mRow,mCol);
    currRainMask= zeros(mRow,mCol);
    for np= 1:spInfo.numSP     % for each SP
        spMask= spInfo.spIm==(np-1); 
        spIdx= find(spMask);  % pixels in SP
        
        [rows,cols]= ind2sub([mRow,mCol],spIdx);       
        rowStart= min(rows);
        rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
        colStart= min(cols);
        colEnd= min(mCol,colStart+spMatchParams.blkSize-1); 
        
        % For each neighbor (nv), extract rectangular patch that best matches current SP and place it in
        % T0(:,:,nv)
        spMatch= currMatch(np).matches;  
        spOffSet= currMatch(np).offsetToSpBox;
        T0= zeros(spMatchParams.blkSize,spMatchParams.blkSize,sizeBuffer);
        for nv=1:sizeBuffer
            currIdx= nv*10;
            r0= spMatch(3,currIdx)+spOffSet(1)+1;
            r1= min(mRow,r0+spMatchParams.blkSize-1);
            c0= spMatch(4,currIdx)+spOffSet(2)+1;
            c1= min(mCol,c0+spMatchParams.blkSize-1);           
            T0(1:(r1-r0+1),1:(c1-c0+1),nv)= rainBufferY(r0:r1,c0:c1,referenceIdx(nv));           
        end
        
        % Pixel marked as rain if there are three or more neighbor patches where processed SP's pixel 
        % intensity is higher than neighbor patches's pixel intensity by 3/255.
        accMask= sum((repmat(T0(:,:,sizeBuffer),1,1,sizeBuffer)-T0)>(3/255),3)>=3;
        
        % Copy rain pixels within area of SP from accMask to rainMask. accMask is a rectangular area
        % larger than SP
        currRainMask(rowStart:rowEnd, colStart:colEnd)= ...
                accMask(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));    
        rainMask(spIdx)= currRainMask(spIdx);
    end
    
    % Apply morphological opening
%     se2 = strel('arbitrary',[0,1,0;0,1,0;0,1,0]); 
%     rainMask= imopen(rainMask,se2);  
    
    % For each SP, find number of rain pixels (normalize tp area), and take the median value.
    for np= 1:spInfo.numSP     
        spMask= spInfo.spIm==(np-1); 
        spIdx= find(spMask);
        currSpIntensity(np)= length(find(rainMask(spIdx)))/length(spIdx);    
    end
    medIntensity= median(currSpIntensity);
    
    IntenRec(ni,1)= currInten;
    IntenRec(ni,2)= medIntensity;
    
    imwrite(uint8(rainMask)*255,[outputFolder,'rainMasks/',pngList(ni-2).name],'png'); 

    fprintf(repmat('\b',1,nCount));
    nCount= fprintf('Current Frame %d', ni);
        
end
IntenRec = IntenRec(5:end,:);
x = IntenRec(:,2); y = IntenRec(:,1);

save([outputFolder 'IntenRec.mat'],'IntenRec');

%% Training
% p = polyfit(x,y,1);
% save([outputFolder 'p.mat'],'p');

%% Testing
load('..\results\MIX_F1_roundAbout_01_random\derainSPAC\p.mat');
% % load('IntenRec.mat'); % from testing folder or from current test

%% check fitting and plot results
x = IntenRec(:,2); y = IntenRec(:,1);
yEst = p(1)*x + p(2);
% figure; plot(y,x,'o',yEst,x,'-')
yresid = yEst - y;
rsq = 1 - sum(yresid.^2)/((length(y)-1) * var(y))

RMSE = sqrt(mean(yresid.^2))
MAE = mean(abs(yresid))
mean_err = mean(yresid)

% plot
for n=1:7
    temIdx= find(y==n);
    meanVec(n)= mean(yEst(temIdx));
    stdVec(n)= std(yEst(temIdx));
    var_yEst(n) = var(yEst(temIdx));
end

mean_std_y = mean(stdVec)
% mean_variance_yEst = mean(var_yEst)

figure;
e = errorbar(1:7,meanVec,stdVec,'-s','MarkerSize',10,...
    'MarkerEdgeColor','red','MarkerFaceColor','red');
ax=gca;ax.XLim = [-1 8];ax.YLim = [-1 8];
xlabel('Ground truth rain intensity','FontSize',16)
ylabel('Estimated rain intensity','FontSize',16)
% export_fig([outputFolder 'graph.png']);
export_fig('graph.png');