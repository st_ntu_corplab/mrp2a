clear;
warning off;

addpath(genpath('required/'));

% Read data files
inputFolder= '..\Database\';
vList= dir([inputFolder,'\MIX*']);
for nn=1:length(vList)
    fprintf([num2str(nn),'. ',vList(nn).name,'\n']);
end
vNo= input('Choose Video Sequence: ');
fileFolder= [inputFolder,vList(vNo).name,'\'];
pngList= dir([fileFolder,'\*.png']);   
numFrames= length(pngList);    
[mRow,mCol,nCC]= size(imread([fileFolder,'/',pngList(1).name]));

outputFolder= ['..\results\' vList(vNo).name '\derainAlign\'];
mkdir([outputFolder,'rainMasks/']);

% Store data for 5 consecutive frames in buffers. Processed frame is middle frame.
sizeBuffer= 5;
rainBuffer= zeros(mRow,mCol,nCC,sizeBuffer); % frame
rainBufferY= zeros(mRow,mCol,sizeBuffer); % frame (grayscale)
rainIntensityRec= zeros(1,sizeBuffer); % groundtruth rain intensity value
nCount= 0; % processed frame number

% Run program
% partially fill buffer
for ni= 1:(sizeBuffer-1)
    newFrame= im2double(imread([fileFolder,'/',pngList(ni).name]));
    rainBuffer(:,:,:,ni+1)= rgb2ycbcr(newFrame);
    rainBufferY(:,:,ni+1)= squeeze(rainBuffer(:,:,1,ni+1));
    
    % update rain intensity record in buffer
    strLoc= strfind(pngList(ni).name,'_');
    newInten= str2num(pngList(ni).name(strLoc(end)+1:end-5));
    rainIntensityRec(ni+1)= newInten;
end

numFrames=149;
for ni=sizeBuffer:numFrames
    
    %  1. input new frame and manage buffer update
    newFrame= im2double(imread([fileFolder,'/',pngList(ni).name]));
    
    % update frame in buffer
    rainBuffer(:,:,:,1:end-1)= rainBuffer(:,:,:,2:end);
    rainBuffer(:,:,:,end)= rgb2ycbcr(newFrame);
    rainBufferY(:,:,1:end-1)= rainBufferY(:,:,2:end);
    rainBufferY(:,:,end)= squeeze(rainBuffer(:,:,1,end));
    
    currFrame= rainBuffer(:,:,:,(sizeBuffer+1)/2); % currently processed frame
    currFrameY= rainBufferY(:,:,(sizeBuffer+1)/2);
    
    % update rain intensity in buffer
    strLoc= strfind(pngList(ni).name,'_');
    newInten= str2num(pngList(ni).name(strLoc(end)+1:end-5));
    rainIntensityRec(1:end-1)= rainIntensityRec(2:end);
    rainIntensityRec(end)= newInten;
    currIntensity= rainIntensityRec((sizeBuffer+1)/2);
    
    % 2. Get rain mask from alignment
    alignedBufferY= zeros(mRow,mCol,sizeBuffer); % Frames in buffer are globally aligned to central frame
    for i=1:sizeBuffer
        tMat = getTransform_KLT(rainBuffer(:,:,:,i),rainBuffer(:,:,:,3),'projective');
        alignedBufferY(:,:,i) = imwarp(rainBufferY(:,:,1), projective2d(tMat),...
            'cubic','OutputView',imref2d([mRow mCol]),'FillValues',0);
    end
    
    alignedBufferY(:,:,3) = [];
    
    % Pixel marked as rain if there are three or more neighbor frames where processed frame's (currFrameY) pixel 
    % intensity is higher than neighbor frame's pixel intensity by 3/255.
    rainMask= sum((repmat(currFrameY,1,1,sizeBuffer-1)-alignedBufferY)>(3/255),3)>=3; 
    
%     se = strel('disk',1);
%     rainMask= imopen(rainMask,se);
    
    IntenRec(ni,1)= currIntensity;
    IntenRec(ni,2)= sum(rainMask(:));
    
    imwrite(uint8(rainMask)*255,[outputFolder,'rainMasks/',pngList(ni-2).name],'png');

    fprintf(repmat('\b',1,nCount));
    nCount= fprintf('Current Frame %d', ni);
end
IntenRec = IntenRec(5:end,:);
x = IntenRec(:,2); y = IntenRec(:,1);

save([outputFolder 'IntenRec.mat'],'IntenRec');

%% Training
% p = polyfit(x,y,1);
% save([outputFolder 'p.mat'],'p');

%% Testing
load('..\results\MIX_F1_roundAbout_01_random\derainAlign\p.mat'); % from training folder
% % load('IntenRec.mat'); % from testing folder or from current test

%% check fitting and plot results
x = IntenRec(:,2); y = IntenRec(:,1);
yEst = p(1)*x + p(2);
% figure; plot(y,x,'o',yEst,x,'-')
yresid = yEst - y;
rsq = 1 - sum(yresid.^2)/((length(y)-1) * var(y))

RMSE = sqrt(mean(yresid.^2))
MAE = mean(abs(yresid))
mean_err = mean(yresid)

% plot
for n=1:7
    temIdx= find(y==n);
    meanVec(n)= mean(yEst(temIdx));
    stdVec(n)= std(yEst(temIdx));
    var_yEst(n) = var(yEst(temIdx));
end

mean_std_y = mean(stdVec)
% mean_variance_yEst = mean(var_yEst)

figure;
e = errorbar(1:7,meanVec,stdVec,'-s','MarkerSize',10,...
    'MarkerEdgeColor','red','MarkerFaceColor','red');
ax=gca;ax.XLim = [-1 8];ax.YLim = [-1 8];
xlabel('Ground truth rain intensity','FontSize',16)
ylabel('Estimated rain intensity','FontSize',16)
% export_fig([outputFolder 'graph.png']);
export_fig('graph.png');



