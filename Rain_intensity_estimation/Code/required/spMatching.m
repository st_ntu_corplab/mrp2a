function output = spMatching(im, imBuf, f_splabel, f_mask, search_y, search_x, useSpMask, useInputMask)
% Applies template matching for a set of superpixels (SPs) in im, searching for closest matches for each
% SP in each image of imBuf. Works exactly the same as in Derainer.

% f_splabel - a 1-channel image denoting SP segmentations. Each pixel value indicates the index of the SP
%   it belongs to. SP indexing starts from 0.
% f_mask - a 1-channel image indicating pixels in im that are excluded from template matching
% search_y - number of pixels that is searched in the positive/negative y direction. Search window is 
%   larger than template in y dimension by 2*search_y pixels
% search_x - number of pixels that is searched in the positive/negative x direction. Search window is 
%   larger than template in x dimension by 2*search_x pixels
% useSpMask - bool indicating whether SP mask (from f_splabel) is used for template matching. If true, only pixels in 
%   f_splabel are used for template matching. If false, all pixels in enlarged bounding box of SP (enlarged
%   by 50% in x and y dimensions are used.
% useSpMask - bool indicating whether f_mask is used for template matching. If true, only 'true' pixels in 
%   f_mask are used for template matching.

% Pseudocode 
% matchMask = ones(size(enlarged superpixel bounding box))
% spMask = (f_splabel==SP_index)(matchMask_pixels)
% inputMask = f_mask(matchMask_pixels)
% if(useSpMask)
%     matchMask = matchMask & spMask
% if (useInputMask)
%     matchMask = matchMask & inputMask
    
% requires opencv DLLs, visual studio 2015 runtime DLL


% check inputs
assert(isa(im, 'uint8'));
assert(isa(imBuf,'cell'));
assert(isa(imBuf{1},'uint8'));

assert(ndims(im) == ndims(imBuf{1}));

% spLabel
if(~isa(f_splabel,'int32'))
    f_splabel = int32(f_splabel);
end

% change f_mask to 0 or 255
if(~isa(f_mask,'uint8'))
    f_mask = uint8(f_mask);
end

if(any(f_mask(:)==1)) 
    f_mask = f_mask*255;
end
assert(all(f_mask(:)==0 | f_mask(:)==255));

if(~isa(search_x,'int32'))
    search_x = int32(search_x);
end

if(~isa(search_y,'int32'))
    search_y = int32(search_y);
end

if(~islogical(useSpMask))
    useSpMask = logical(useSpMask);
end

if(~islogical(useInputMask))
    useInputMask = logical(useInputMask);
end

output = spMatching_mex(im, imBuf, f_splabel, f_mask, search_y, search_x, useSpMask, useInputMask);

% clear spMatching_mex