% tMat warps im1 to get imgWarp. imgWarp is aligned with im2
function [tMat,imgWarp] = getTransform_KLT(im1,im2, transformType)

% vid = vidread('D:\Dropbox\Work\Rain_removal\Database\rain\postbox.mp4');
% im1 = imread('D:\Dropbox\Work\Rain_removal\results\mindef\v2\postbox\input\0000.jpg');
% im2 = imread('D:\Dropbox\Work\Rain_removal\results\mindef\v2\postbox\input\0001.jpg');
% im1 = vid(:,:,:,1); im1 = imresize(im1,0.5);
% im2 = vid(:,:,:,11);im2 = imresize(im2,0.5);

im_gray1 = rgb2gray(im1);
im_gray2 = rgb2gray(im2);

% Find interest points
points1 = detectMinEigenFeatures(im_gray1);
points1 = points1.Location;
% points1 = points1(1:50:end,:);

% pointImage = insertMarker(im1, points1, '+', 'Color', 'green');
% figure, imshow(pointImage);
% title('Detected feature points');

tracker = vision.PointTracker('MaxBidirectionalError', 1.5);
initialize(tracker, points1, im1);

[points2, validity] = step(tracker, im2);
validpoints2 = points2(validity, :);
validpoints1 = points1(validity, :);

if ~exist('transformType','var')
    transformType = 'affine';
end

[tform, inlierPoints1,inlierPoints2] = estimateGeometricTransform(...
validpoints1,validpoints2, transformType);

% warp
imgWarp = imwarp(im1, tform, 'cubic','OutputView', imref2d(size(im_gray2)));
tMat = tform.T;
% inlierPoints1warp = transformPointsForward(tform, inlierPoints1.Location);

% figure(5); imshow(im1);
% figure(6); imshow(im2);
% figure(7); imshow(imgWarp);
% 
% pointImage = insertMarker(im1, validpoints1, '+', 'Color', 'green');
% figure(8); imshow(pointImage);
% pointImage2 = insertMarker(im2, validpoints2, '+', 'Color', 'green');
% figure(9); imshow(pointImage2);
% figure(10); showMatchedFeatures(im1, im2, inlierPoints1, inlierPoints2, 'montage');

% diff = abs(rgb2gray(im2)-rgb2gray(imgWarp)) > 10/255;
% figure(8); imshow(diff);
% 
% diff = abs(rgb2gray(im2)-rgb2gray(im1)) > 10/255;
% figure(9); imshow(diff);