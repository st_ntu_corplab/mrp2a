Rain intensity estimation
Code
Three matlab implementations are included – the proposed method (derainSpac_imopen.m) and two methods for comparison (derainAlign.m, derainSpac.m). derainSpac.m is the same as derainSpac.m, but without morphological opening. derainAlign.m implements alignment using global alignment instead of superpixel alignment. See paper for more details (Rain Intensity Estimation - Vision-Based Rain Gauge for Dynamic Scenes.pdf). Each matlab implementation runs training, testing, and evaluation of results.

Database
A regression function maps the estimated rain density value from the algorithm (a unitless number) to an actual rain intensity value (mm/hour). We require a set of different actual rain intensity values (for light, medium, heavy rain) for the exact same scenes. Since this data is impractical to collect, we simulate actual rain (ground truth) under different degrees of intensity (labelled 1 to 7, instead of mm/hr) using Adobe Aftereffects.

Each dataset contains an image sequence where the rain intensity of each frame is different. Filename suffix ‘random’ indicates random selection of rain intensity for each frame, while suffix ‘forward’ indicates rain intensity increases by 1 step for each frame until the maximum level, then resets back to lowest intensity, and the process repeats. Each image file is also tagged with a suffix from ‘1K’ (lowest) to ‘7K’ (highest) indicating its intensity level. 

Running the codes
Each implementation outputs a rainMask for each frame (shows rain pixels in black-white image), and a matrix – IntenRec, saved as IntenRec.mat. IntenRec stores the ground truth for each image (row) in 1st column, and the estimated rain intensity in 2nd column.

Training is required to obtain the parameters for rain intensity regression. Training is done on dataset ‘MIX_F1_roundAbout_01_random’. For training, comment out ‘Testing’ code and uncomment ‘Training’ code, and run program on training video ‘roundabout_random’. The ‘Training’ code computes regression coefficients for mapping ground truth to estimated intensity, and saves them to p.mat. ‘Check fitting’ code generates evaluation results.

For testing, comment code ‘Training’ and uncomment code ‘Testing’ to load p.mat, and run program on test video. If the entire program is run, ignore % load('IntenRec.mat'); under ‘Testing’. If the a 'IntenRec.mat’ for the test video is already available, then load that instead.

For both testing and training data, code under ‘Check fitting’ generates graphs and statistics of accuracy of mapping between ground truth and estimated rain intensity.
 
