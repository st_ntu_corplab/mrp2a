clear;
close all;

IN.dataSet= 0; % 0 for the fattal dataset with ground truth
               % 1 for other images without ground true
               % 2 for image sequences from video 
               % 3 for direct video sequence
IN.writeOutput= 0; % 1 for enabling writing output results

% OUT.outputFolder= ['\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Haze\datasetFattal\hazeMeasure\_results\'];

% choose Image
IN.fileFolderPath= ['\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Haze\datasetFattal\hazeMeasure\'];
IN.listIm = dir([IN.fileFolderPath, '..\*.mat']);
for nf=1:length(IN.listIm)
    fprintf(['%d. ',IN.listIm(nf).name(1:end-16),'\n'],nf);
end 
numI= input('Please chose image: '); 
IN.currFileName= IN.listIm(numI).name(1:end-16);

% find same images
IN.list = dir([IN.fileFolderPath,IN.currFileName,'\*',IN.currFileName,'*.png']);
% choose anchor
 
for nf=1:length(IN.list)
    fprintf(['%d. ',IN.list(nf).name,'\n'],nf);
end 
numRef= input('Please chose reference [r1, r2]: ');

IN.refFolderPath= IN.fileFolderPath;
IN.refNames(1)= {IN.list(numRef(1)).name};
IN.refNames(2)= {IN.list(numRef(2)).name};
% remove reference from the testing data
IN.list([numRef(1),numRef(2)])= [];
IN.numContents= length(IN.list);
 
scatterVec= [0.5:0.1:1.5];
airlightVec= zeros(5,1,3);
na=1;
airlightVec(na,1,:)= [0.5,0.6,1]; na=na+1;
airlightVec(na,1,:)= [219, 226, 255]/256; na=na+1;
airlightVec(na,1,:)= [153, 255, 198]/256; na=na+1;
airlightVec(na,1,:)= [234, 253, 255]/256; na=na+1;
airlightVec(na,1,:)= [217, 219, 188]/256; na=na+1;
% airlightVec(na,1,:)= [182, 193, 192]/256; na=na+1;
% airlightVec(na,1,:)= [255, 253, 183]/256; na=na+1;
% for na= 1:length(airlightVec)
%     airlightVec(na,1,:)= airlightVec(na,1,:)/norm(squeeze(airlightVec(na,1,:)),2);
% end
%{
showFig= [];
showFig(:,:,1)= kron(airlightVec(:,:,1),ones(100,100));
showFig(:,:,2)= kron(airlightVec(:,:,2),ones(100,100));
showFig(:,:,3)= kron(airlightVec(:,:,3),ones(100,100));
figure; imshow(showFig);
%}

% all sequence
% numF= [1:IN.numContents]; 

for nf=1:length(IN.list)
    fprintf(['%d. ',IN.list(nf).name,'\n'],nf);
end 
numF= input('Please target image: ');

for nn=1:length(numF)

% read anchor parameters 
Iref1= im2double(imread([IN.refFolderPath,IN.currFileName,'\',IN.refNames{1}]));
Iref2= im2double(imread([IN.refFolderPath,IN.currFileName,'\',IN.refNames{2}]));

sc1= str2num(IN.refNames{1}(end-11:end-9));
sc2= str2num(IN.refNames{2}(end-11:end-9));

a1= airlightVec(str2num(IN.refNames{1}(end-5)),1,:);
a2= airlightVec(str2num(IN.refNames{2}(end-5)),1,:);
% figure;imshow([repmat(a1,100,100,1),repmat(a2,100,100,1)]);

%{ 
% testing for the references
%% calculate DCP
darkChanref1= calDCP(Iref1,7); %darkChan
darkChanref2= calDCP(Iref2,7); %darkChan
% figure;imshow(darkChan);

%% estimate airlight
[A1, locA1] = EstimateAirLight(darkChanref1,Iref1);
[A2, locA2] = EstimateAirLight(darkChanref2,Iref2);
% figure;imshow([repmat(A1,100,100,1),repmat(A2,100,100,1)]);
%}

%% load testing data
I= im2double(imread([IN.fileFolderPath,IN.currFileName,'\',IN.list(numF(nn)).name]));
% read ground truth parameters
scTrue= str2num(IN.list(numF(nn)).name(end-11:end-9));
aTrue= airlightVec(str2num(IN.list(numF(nn)).name(end-5)),1,:);
% figure;imshow(repmat(aTrue,100,100,1));

% calculate DCP
darkChan= calDCP(I,7); %darkChan
% figure;imshow(darkChan);

%% estimate airlight
[A, locA] = EstimateAirLight(darkChan,I);
% figure;imshow(repmat(A,100,100,1));

spInfo.NP= floor(size(I,1)*size(I,2)/1000); 
[spInfo.spIm, spInfo.numSP]= slicmex(im2uint8(I),spInfo.NP,50);
%{
[Gx,Gy]= gradient(double(spInfo.spIm));
SegBG= ((Gx.^2+Gy.^2)~=0);
figure;imshow(SegBG+im2double(rgb2gray(I)));
%}

[mRow,mCol,nCC]= size(Iref1);

left= (Iref2-repmat(a2,mRow,mCol,1))./Iref1;
right= repmat(a2,mRow,mCol,1).*(1./repmat(a1,mRow,mCol,1)-1./Iref1);
logVal= left./right; % figure;imshow(-[left,right])
betaD= -log(logVal);
betaDGref= rgb2gray(betaD);

left= (I-repmat(A,mRow,mCol,1))./Iref1;
right= repmat(A,mRow,mCol,1).*(1./repmat(a1,mRow,mCol,1)-1./Iref1);
logVal= left./right; % figure;imshow(-[left,right])
betaD= -log(logVal);
betaDG= rgb2gray(betaD);
% figure;imshow([betaDG,betaDGref]);

%{
[Gx,Gy]= gradient(double(spInfo.spIm));
SegBG(:,:,1)= double(((Gx.^2+Gy.^2)~=0));
SegBG(:,:,2:3)= repmat(zeros(mRow,mCol),1,1,2);
figure;imshow(repmat(SegBG,1,2,1)+(1-repmat(SegBG(:,:,1),1,2,3)).*repmat([betaDG,betaDGref],1,1,3));
imwrite(repmat(SegBG,1,2,1)+(1-repmat(SegBG(:,:,1),1,2,3)).*repmat([betaDG,betaDGref],1,1,3),...
            sprintf('%.2fsc%da.png',scTrue,str2num(IN.list(numF(nn)).name(end-5))),'png')
%}

betaDGvec_tar=[]; 
betaDGvec_ref=[];
darkChan_vec=[];
for np= 1:spInfo.numSP     
    spMask= spInfo.spIm==(np-1); 
    spIdx= find(spMask);

    betaDGvec_ref(np)= median(betaDGref(spIdx));
    betaDGvec_tar(np)= median(betaDG(spIdx));
    
    darkChan_vec(np)= median(darkChan(spIdx));
end

darkSP= find(darkChan_vec<=0.5);
% [val,idx]= sort(darkChan_vec,'ascend');
% darkSP= idx(1:floor(0.01*spInfo.numSP));

scRatioVec= betaDGvec_tar(darkSP)./betaDGvec_ref(darkSP); % figure;plot(scRatioVec);
scRatioVec(find(isnan(scRatioVec)))=[];
scRatio= median(scRatioVec);

beta= sc1+(sc2-sc1)*scRatio;

testResultVec(nn,1)= beta;
testResultVec(nn,2)= scTrue;
testResultVec(nn,3)= sc1;
testResultVec(nn,4)= sc2;
testResultVec(nn,5:7)= a1;
testResultVec(nn,8:10)= a2;

scatter(testResultVec(:,2),testResultVec(:,1),'filled','r');axis([0.4,1.6 0.4,1.6]);
drawnow

end

%{
save([OUT.outputFolder,sprintf('%s_%.1fsc%da_.%.1fsc%da.mat',...
        IN.currFileName,sc1,str2num(IN.refNames{1}(end-5)),...
        sc2,str2num(IN.refNames{2}(end-5)))],'testResultVec')
%}



% if(IN.writeOutput==1)
%     if(IN.dataSet==0)
%         if(~exist([IN.fileFolderPath,IN.list(n).name(1:end-4),'_output']))
%             mkdir([IN.fileFolderPath,IN.list(n).name(1:end-4),'_output']);
%         end
%         imwrite(J,[IN.fileFolderPath,IN.list(n).name(1:end-4),'_output\','J_mine.png'],'png');
%         imwrite(Jnl,[IN.fileFolderPath,IN.list(n).name(1:end-4),'_output\','Jnl.png'],'png');
%         imwrite(TNLReg,[IN.fileFolderPath,IN.list(n).name(1:end-4),'_output\','Tnl.png'],'png');
%         imwrite(TReg,[IN.fileFolderPath,IN.list(n).name(1:end-4),'_output\','T_mine.png'],'png');
% 	if(IN.dataSet==1 || IN.dataSet==2 ) 
%         if(~exist([IN.fileFolderPath,'output']))
%             mkdir([IN.fileFolderPath,'output']);
%         end
%         imwrite(J,[IN.fileFolderPath,'output\',fileName,'_dehazed.png'],'png');
%         %imwrite(TReg,[IN.fileFolderPath,'output\',fileName,'_tmap.png'],'png');
%     elseif(IN.dataSet==3)  
%         Jnl= dehazing(I,A,Tnl);
%       fwrite= [I,J];       
%         step(hvfw, fwrite);
%     end
%             
% end
% 
% end
% end
% 
% if(IN.dataSet==3)        
%     release(hvfrInput);
%     release(hvfw);       
% end


