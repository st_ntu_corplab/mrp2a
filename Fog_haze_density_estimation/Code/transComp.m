function Tcomp= transComp(X,Y,A,xVec,yVec)

% quantization
resX= xVec(2)-xVec(1);
rangeX= length(xVec);
Xq= ceil(X/resX);
Xq(Xq<1)=1;
Xq(Xq>rangeX)=rangeX;


resY= yVec(2)-yVec(1);
rangeY= length(yVec);
Yq= ceil(Y/resY);
Yq(Yq<1)=1;
Yq(Yq>rangeY)=rangeY;


idx= sub2ind([rangeX,rangeY],Xq(:),Yq(:));
Tcomp= reshape(A(idx),size(X,1),size(X,2));

end