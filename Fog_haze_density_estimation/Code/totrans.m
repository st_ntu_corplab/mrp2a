function T=totrans(I)

C = tojet([0:0.0001:1]',0,1) ;

len = size(C,1) ;
v = (1:len)' ;
C = reshape(C,len,3) ;

[w h c] = size(I) ;

for x=1:w
    for y=1:h
        
        ind = (C(:,1) == I(x,y,1))&(C(:,2) == I(x,y,2))&(C(:,3) == I(x,y,3)) ;
        
        f = find(ind) ;
        
        if(length(f) > 0)
            T(x,y) = f(1)/ len ;
        else
            T(x,y) = 0 ;
        end
    end
end

%%%%%%%

function I = tojet(F,min,max)

[w h] = size(F) ;

F = (F - min) / (max - min) ;

cr = 0.75 ;
cg = 0.5 ;
cb = 0.25 ;

Ir = 1.5 - 4* abs(F - cr) ;
Ig = 1.5 - 4* abs(F - cg) ;
Ib = 1.5 - 4* abs(F - cb) ;

Ir(F < 0) = 1 ;
Ig(F < 0) = 1 ;
Ib(F < 0) = 1 ;

I = zeros(w,h,3) ;

I(:,:,1) = Ir ;
I(:,:,2) = Ig ;
I(:,:,3) = Ib ;

I(I>1) = 1 ;
I(I<0) = 0 ;

I = uint8(I * 255) ;