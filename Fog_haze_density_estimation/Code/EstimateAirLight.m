function [Airlight, LocA]= EstimateAirLight(DarkChan, IMG)
    % the median of all the 0.1% pixels with largest dark channel values.
    [wid, len, cc]=size(IMG);
    Num= floor(0.001*wid*len);
    
    II= rgb2gray(IMG);
    [gx,gy]= gradient(II);
    gxy= abs(gx)+ abs(gy); 
    gxy= gxy/max(gxy(:)); % figure;imshow(gxy);
    
    h = fspecial('average',[7,7]);
    GI = imfilter(gxy,h,'replicate');% figure;imshow(GI);
    GI= 1- GI/max(GI(:)); % figure;imshow(GI);
    
    a=1;
    b=1;
    c=1;
    
    target= a*DarkChan+ b*GI+ c*II; % figure;imshow(target/max(target(:))); 
    
    
    [Sorted, Ind]= sort(target(:), 'descend');    
    
    currIdx= Ind(round(Num));
    [LocA(2),LocA(1)] = ind2sub([wid, len],currIdx);
    
    Airlight= IMG(LocA(2),LocA(1),:);
        
end

%% End of File