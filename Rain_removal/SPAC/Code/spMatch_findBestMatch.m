function [viewBestMatch,spInfo]= spMatch_findBestMatch(currFrame,...
                            viewBuffer,spInfo,spMatchParams)

[mRow,mCol,nCC,numViews]= size(viewBuffer);
sizeBuffer= numViews;

currFrameY= rgb2gray(currFrame);
for nv=1:numViews
    viewBufferY(:,:,nv)= rgb2gray(viewBuffer(:,:,:,nv));
end

currBestMatch= zeros(mRow,mCol,nCC,'uint8');
currBestMatchY= zeros(mRow,mCol,'uint8');
viewBestMatch= zeros(mRow,mCol,nCC,'uint8');
iewBestMatchY= zeros(mRow,mCol,'uint8');
currAvgMatch= zeros(mRow,mCol,'uint8');
viewAvgMatch= zeros(mRow,mCol,'uint8');

%% SP segmetnation for current frame
[spInfo.spIm,spInfo.numSP]= slicmex(im2uint8(currFrame),spInfo.NP,spInfo.paramReg);
%{
[Gx,Gy]= gradient(double(spInfo.spIm));
SegBG= ((Gx.^2+Gy.^2)~=0);
figure;imshow(repmat(SegBG,1,1,size(currFrame,3))+im2double(currFrame));
%}
%% First round of matching, with SP mask
spMatchParams.useSpMask = 1;
spMatchParams.useInputMask = 0;
spMatchParams.f_splabel= int32(spInfo.spIm);
spMatchParams.f_mask= uint8(ones(mRow,mCol));    

referenceIdx= [1,2,3,5,6,7,4];

currMatch = spMatching_mine(currFrameY, viewBufferY(:,:,referenceIdx(1:end-1)), spMatchParams);   

for np= 1:spInfo.numSP     
    spMask= spInfo.spIm==(np-1); 
    spMaskRGB= uint8(repmat(spMask,1,1,3)); 
    spIdx= find(spMask);

    [rows,cols]= ind2sub([mRow,mCol],spIdx);       
    rowStart= min(rows);
    rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
    colStart= min(cols);
    colEnd= min(mCol,colStart+spMatchParams.blkSize-1); 
    % matching details for current SP
    spMatch= currMatch(np).matches;  
    spOffSet= currMatch(np).offsetToSpBox;
    T0= zeros(spMatchParams.blkSize,spMatchParams.blkSize,sizeBuffer,'uint8');
    T0RGB= zeros(spMatchParams.blkSize,spMatchParams.blkSize,nCC,sizeBuffer,'uint8');
    for nv=1:sizeBuffer
        currIdx= nv*10;
        r0= spMatch(3,currIdx)+spOffSet(1)+1;
        r1= min(mRow,r0+spMatchParams.blkSize-1);
        c0= spMatch(4,currIdx)+spOffSet(2)+1;
        c1= min(mCol,c0+spMatchParams.blkSize-1);           
        T0(1:(r1-r0+1),1:(c1-c0+1),nv)= viewBufferY(r0:r1,c0:c1,referenceIdx(nv));
        T0RGB(1:(r1-r0+1),1:(c1-c0+1),:,nv)= viewBuffer(r0:r1,c0:c1,:,referenceIdx(nv));
    end
    
	vecErr= spMatch(1,10:10:end);
    [minVal,minIdx]= min(vecErr(1:end-1));
    
    currBestMatchY(rowStart:rowEnd, colStart:colEnd)= ...
                T0(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1),minIdx);    
	viewBestMatchY(spIdx)= currBestMatchY(spIdx); 
    
    currBestMatch(rowStart:rowEnd, colStart:colEnd,:)= ...
                T0RGB(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1),:,minIdx);    
	viewBestMatch= viewBestMatch+ currBestMatch.*spMaskRGB; 
    
%     currAvgMatch(rowStart:rowEnd, colStart:colEnd)= ...
%                 mean(T0(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1),:),3);    
% 	viewAvgMatch(spIdx)= currAvgMatch(spIdx); 
    
end

end
