% function showLFIM(SV,indexVec)
function [output]= showLFIM(SV,varargin)

[~,~,v1,v2]=size(SV);

if(length(varargin)==0)
    showAll=1;
elseif(length(varargin)==1)
    showAll=0;
else
    disp('number of input wrong');
end
    
% color LFIM
if(v1==3 & v2~=1)
    if(showAll)
        showTemp= [];
        indexVec= [1:size(SV,4)];
        for v= indexVec
            showTemp= [showTemp,SV(:,:,:,v)];
        end
        figure;imshow(showTemp);
        output= showTemp;
    else
        if(max(indexVec)>v2)
            disp('index wrong');
            return;
        end
        for v= indexVec
            figure;imshow(SV(:,:,:,v));
        end
        output= showTemp;
    end
end

% greyscale LFIM
if(v2==1 & v1~=1)    
    if(showAll)
        showTemp= [];
        indexVec= [1:size(SV,3)];
        for v= indexVec
            showTemp= [showTemp,SV(:,:,v)];
        end
        figure;imshow(showTemp);
        output= showTemp;
    else
        if(max(indexVec)>v1)
            disp('index wrong');
            return;
        end
        for v= indexVec
            figure;imshow(SV(:,:,v));
        end
        output= showTemp;
    end
 end

end