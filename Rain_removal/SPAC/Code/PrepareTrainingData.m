% function PrepareTrainingData()
clear;

addpath(genpath('required/'));

InitParam();
global param;

videoFolder= '\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Rain\_Simulation\';

files = dir([videoFolder,'RAIN_*1*']);
VideoList = files; % exclude . and ..
for nf=1:length(VideoList)
    fprintf(['%d. ',VideoList(nf).name,'\n'],nf);
end 
numF= input('chose rain sequence: ');
% sceneFolder = param.trainingScenes;
% outputFolder = param.trainingData;
outputFolder= videoFolder;

numScenes= length(numF);

if (~exist(outputFolder, 'dir'))
    mkdir(outputFolder);
end

%%
sizeBuffer= 5;
% rainBuffer= zeros(mRow,mCol,nCC,sizeBuffer);
% rainBufferY= zeros(mRow,mCol,sizeBuffer);
% rainMask= zeros(mRow,mCol);
% currRainMask= zeros(mRow,mCol);
% rainRemove= zeros(mRow,mCol,3);
% rainCompensateY= zeros(mRow,mCol);
% currRainCompensateY= zeros(mRow,mCol);
% rainCompensate= zeros(mRow,mCol);
% currSPCompensate= zeros(mRow,mCol);
referenceIdx= [1,2,4,5,3];
numMatch= 10;
%%

numFramesToWork= 21;
numFramesPerBatch= 3; % 3 frames will be save into a batch file

for nf = 1 : numScenes
% for ns = [1]
    ns= numF(nf);
    
    nBatch= 0;
    % set up training parameters
    spInfo.NP= 288; 
    spMatchParams.blkSize= 80; 
    spMatchParams.search_y= 30;
    spMatchParams.search_x= 30;
    
    fprintf('**********************************\n');
    fprintf('Working on the "%s" dataset (%d of %d)\n', VideoList(ns).name, ns, numScenes);
    
	%% First of all, find the GT folder
    strlocation=strfind(VideoList(ns).name,'_');
    gtList= dir([videoFolder, 'GT_', VideoList(ns).name(strlocation(1)+1:strlocation(end)-1),'*']);
    if(length(gtList)~=1)
        disp('wrong GT folder');
        return;
    end
    gtFileName= gtList(1).name;
    %%
    
    frameList = dir([videoFolder,VideoList(ns).name,'\*.png']);
    numFrames= length(frameList);
    % numFramesToWork= floor(numFrames/sizeBuffer);
    % frameIdxVec= 1:sizeBuffer:(numFrames-sizeBuffer+1);    
    frameIdxVec= 1:floor(numFrames/numFramesToWork):(numFrames-sizeBuffer+1);
    frameIdxVec= frameIdxVec(1:numFramesToWork);
    
    npatch= 0;  % patch counter  
    ntext=0;
    for nf= frameIdxVec
        % fill the buffer
        bufferIm=[];
        for nb=1:sizeBuffer
            rainBuffer(:,:,:,nb)= rgb2ycbcr(im2uint8(imread([videoFolder,VideoList(ns).name,...
                '\',frameList(nf+referenceIdx(nb)-1).name])));
            rainBufferY(:,:,nb)= squeeze(rainBuffer(:,:,1,nb));
        end
        
        bufferIm= im2uint8(rainBufferY(:,:,1:sizeBuffer-1));
        targetIm= im2uint8(rainBufferY(:,:,sizeBuffer));
        %% extra allocation
        [mRow,mCol,nCC]= size(targetIm);
        currRainMask= zeros(mRow,mCol);
        rainMask= zeros(mRow,mCol);
        
        %% calculate UV edges for target frame
        [Ux,Uy]= gradient(im2double(rainBuffer(:,:,2,sizeBuffer)));
        [Vx,Vy]= gradient(im2double(rainBuffer(:,:,3,sizeBuffer)));
        currUV= mean(abs(Ux)+abs(Uy)+abs(Vx)+abs(Vy),3)>0.01; % figure;imshow(currUV);

        %% SP segmetnation for current frame
        [spInfo.spIm, spInfo.numSP]= slicmex(im2uint8(targetIm),spInfo.NP,30);
        %{
        [Gx,Gy]= gradient(double(spInfo.spIm));
        SegBG= ((Gx.^2+Gy.^2)~=0);
        figure;imshow(SegBG+im2double(targetIm));
        %}
        npatchT0= npatch;
        %% First round of matching, with SP mask and rain mask
        spMatchParams.useSpMask = 0;
        spMatchParams.useInputMask = 0;
        spMatchParams.f_splabel= int32(spInfo.spIm);
        spMatchParams.f_mask= uint8(ones(mRow,mCol));    
        currMatch = spMatching_mine(targetIm, bufferIm, spMatchParams);       
        for np= 1:spInfo.numSP     
            spMask= spInfo.spIm==(np-1); 
            spIdx= find(spMask);

            [rows,cols]= ind2sub([mRow,mCol],spIdx);       
            rowStart= min(rows);
            rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
            colStart= min(cols);
            colEnd= min(mCol,colStart+spMatchParams.blkSize-1); 
            % matching details for current SP
            spMatch= currMatch(np).matches;  
            spOffSet= currMatch(np).offsetToSpBox;
            T0= zeros(spMatchParams.blkSize,spMatchParams.blkSize,sizeBuffer,'uint8');
            for nv=1:sizeBuffer
                currIdx= nv*10;
                r0= spMatch(3,currIdx)+spOffSet(1)+1;
                r1= min(mRow,r0+spMatchParams.blkSize-1);
                c0= spMatch(4,currIdx)+spOffSet(2)+1;
                c1= min(mCol,c0+spMatchParams.blkSize-1);           
                % T0(1:(r1-r0+1),1:(c1-c0+1),nv)= rainBufferY(r0:r1,c0:c1,referenceIdx(nv)); % previously wrong
                T0(1:(r1-r0+1),1:(c1-c0+1),nv)= rainBufferY(r0:r1,c0:c1,nv);
            end
            accMask= sum((repmat(T0(:,:,sizeBuffer),1,1,sizeBuffer)-T0)>0.005,3)>=3;                  

            currRainMask(rowStart:rowEnd, colStart:colEnd)= ...
                    accMask(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));    
            rainMask(spIdx)= currRainMask(spIdx); % figure;imshow(rainMask);
            
            npatchT0= npatchT0+1;      
            INT0(:,:,:,npatchT0)          = T0;
        end
        rainMask= rainMask.*(~currUV);

        %% second round of matching, with SP mask and rain mask
        spMatchParams.useSpMask = 1;
        spMatchParams.useInputMask = 1;
        spMatchParams.f_splabel= int32(spInfo.spIm);
        spMatchParams.f_mask= uint8(~rainMask);    
        currMatch2 = spMatching_mine(targetIm, bufferIm, spMatchParams);    
        for np= 1:spInfo.numSP     
            spMask= double(spInfo.spIm==(np-1));
            spIdx= find(spMask);

            [rows,cols]= ind2sub([mRow,mCol],spIdx);       
            rowStart= min(rows);
            rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
            colStart= min(cols);
            colEnd= min(mCol,colStart+spMatchParams.blkSize-1); 
            % matching details for current SP
            spMatch= currMatch2(np).matches;  
            spOffSet= currMatch2(np).offsetToSpBox;                
            T1= zeros(spMatchParams.blkSize,spMatchParams.blkSize,numMatch,'uint8');  
            CurrPatch= zeros(spMatchParams.blkSize,spMatchParams.blkSize,3,'uint8');  
            RAINMASK= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'double');
            SPMASK= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'double');
            GTFRAME= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'uint8');
            [errorVal,errorIdx]= sort(spMatch(1,1:40),'ascend');
            % [~,errorIdx]= sort(spMatch(1,1:numMatch*(sizeBuffer-1)),'ascend');
            ne=0;
            for ni=1:numMatch
                if(errorVal(ni)<0.1)
                    ne=ne+1;
                end
                currIdx= errorIdx(ni);
                nv= ceil(currIdx/10);
                r0= spMatch(3,currIdx)+spOffSet(1)+1;
                r1= min(mRow,r0+spMatchParams.blkSize-1);
                c0= spMatch(4,currIdx)+spOffSet(2)+1;
                c1= min(mCol,c0+spMatchParams.blkSize-1);                        
                T1(1:(r1-r0+1),1:(c1-c0+1),ni)= rainBufferY(r0:r1,c0:c1,nv);  
            end
            
            %% this part is only for data preparation
            % 0. prepare current patch
            CurrPatch(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1),:)...
                = rainBuffer(rowStart:rowEnd, colStart:colEnd,:,sizeBuffer);
            % 1. extract rain map
            RAINMASK(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1))= ...
                                rainMask(rowStart:rowEnd, colStart:colEnd);
            % 2. extract SP mask
            SPMASK(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1))= ...
                                spMask(rowStart:rowEnd, colStart:colEnd);
            % 3. read ground truth patch
            gtCurrFrame= im2uint8(imread([videoFolder,gtFileName,'\',...
                                frameList(nf+referenceIdx(sizeBuffer)-1).name]));
            gtCurrFrameY= rgb2ycbcr(gtCurrFrame);
            gtCurrFrameY= gtCurrFrameY(:,:,1);
            GTFRAME(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1))= ...
                        gtCurrFrameY(rowStart:rowEnd, colStart:colEnd);          
            % imshow([showLFIM(T1)/255,GTFRAME/255]);    
            npatch= npatch+1;      
            INDATA(:,:,:,npatch)        = im2uint8(CurrPatch); 
            INT1(:,:,:,npatch)          = T1; 
            INRAINMASK(:,:,1,npatch)	= im2uint8(RAINMASK);
            INSPMASK(:,:,1,npatch)      = im2uint8(SPMASK);
            GTIM(:,:,1,npatch)          = GTFRAME;
            %{
            nt=230;
            imshow([repmat(showLFIM(INT1(:,:,:,nt)),1,1,3),repmat(INRAINMASK(:,:,:,nt),1,1,3),...
                    repmat([INSPMASK(:,:,:,nt),GTIM(:,:,:,nt)],1,1,3),INDATA(:,:,:,nt),...
                    repmat(showLFIM(INT0(:,:,:,nt)),1,1,3)]); 
            %}

            % if(ne<5)
            %     currSPCompensate= mean(T1(:,:,1:5),3);
            % else
            %     currSPCompensate= mean(T1,3);
            % end
            % currRainCompensateY(rowStart:rowEnd, colStart:colEnd)= ...
            %         currSPCompensate(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));    
            % rainCompensateY(spIdx)= currRainCompensateY(spIdx); % figure;imshow(rainCompensateY);        
        
        end 
        
        msg= sprintf('processing frame %d of %d',find(nf==frameIdxVec),numFramesToWork);
        fprintf(repmat('\b',1,ntext));
        fprintf(msg);
        ntext=numel(msg);
        
        if(mod(find(nf==frameIdxVec),numFramesPerBatch)==0)
            npatch= 0;
            nBatch= nBatch +1;
            fprintf(repmat('\b',1,ntext));
            fprintf('Completed batch No. %d of data \"%s\"\n', nBatch, VideoList(ns).name);
            msg= sprintf('Writing training examples  ...\n');fprintf(msg);        
            ntext=numel(msg);
            save([outputFolder,'Training2_',VideoList(ns).name,sprintf('_b%.3d.mat',nBatch)],...
                'INDATA','INT1','INRAINMASK','INSPMASK','GTIM','INT0');        
            clear INDATA INT1 INRAINMASK INSPMASK GTIM INT0;  
        end
                       
    end
    fprintf(repmat('\b',1,ntext));       
    fprintf('**********************************\n'); 
    
end
     
    
    
   
 