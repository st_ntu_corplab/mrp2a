clearvars; clearvars -global; close all;
% warning off;
addpath(genpath('required/'));
outputFlag = 1;

% Setup CNN
% vl_setupnn();
% InitSystemParam();
% global param;
% param.testNet= 'testNet\';
% netList= dir([param.testNet,'*.mat']);
% load([param.testNet,netList(end).name]);

% Get files
contentFolder = 'D:\Dropbox\Work\Rain_removal\Database\rain\selectedTests';
% inputFolder = 'E:\Database\rain\_Simulation_Intensity\RAIN_1k_roundAbout_01\';
outputFolder = 'D:\Dropbox\Work\Rain_removal\results\temptest\temp\';

folderList= dir(contentFolder);
folderList=folderList(~ismember({folderList.name},{'.','..'}));
files = arrayfun(@(x)(fullfile(x.folder,x.name)),folderList,'uniformoutput',false);

for itF=1:numel(files)
    vIn = vidread(files{itF});
%     vIn = vidread(files{itF},1,7);
    outputData = apply_derainNetwork(vIn);
    
    [~,filename,~] = fileparts(files{itF});
    fields = fieldnames(outputData);
    for i = 1:numel(fields)
        data = outputData.(fields{i});
        vidwrite(data,[outputFolder filename '\' fields{i} '\.png']);  
    end 
end






