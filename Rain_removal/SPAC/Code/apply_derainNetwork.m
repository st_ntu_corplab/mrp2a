% clearvars; clearvars -global; close all;
% % warning off;
% addpath(genpath('required/'));
% outputFlag = 1;
% 
% % Setup CNN
% % vl_setupnn();
% % InitSystemParam();
% % global param;
% % param.testNet= 'testNet\';
% % netList= dir([param.testNet,'*.mat']);
% % load([param.testNet,netList(end).name]);
% 
% % Get files
% % contentFolder = 'D:\Dropbox\Work\Rain_removal\Database\rain\selectedTests';
% inputFolder = 'E:\Database\rain\_Simulation_Intensity\RAIN_1k_roundAbout_01\';
% outputFolder = 'D:\Dropbox\Work\Rain_removal\results\temptest\temp\';
% % folderList= dir(inputFolder);
% % folderList=folderList(~ismember({folderList.name},{'.','..'}));
% % files = arrayfun(@(x)(fullfile(x.folder,x.name)),folderList,'uniformoutput',false);
% 
% vIn = vidread(inputFolder,1,7);
% output = derainNetwork(vIn);

function output = apply_derainNetwork(vidIn)
    
    % input checking
    assert(ndims(vidIn) == 4);
    assert(isa(vidIn, 'uint8'));
    
    % params
    spInfo.NP= 288;
    spMatchParams.blkSize= 80;
    spMatchParams.search_y= 30;
    spMatchParams.search_x= 30;
    
    numMatch= 10;
    sizeBuffer= 5;
    referenceIdx= [1,2,4,5,3];
    
    [mRow,mCol,nCC,numFrames]= size(vidIn);
    
    % output
    output.rainRemoveAvg1 = zeros(mRow,mCol,nCC,numFrames,'uint8');
    output.rainRemoveAvg = zeros(mRow,mCol,nCC,numFrames,'uint8');
    output.rainRemove_dir = zeros(mRow,mCol,nCC,numFrames,'uint8');
    output.rainRemove = zeros(mRow,mCol,nCC,numFrames,'uint8');
     
    rainBuffer= zeros(mRow,mCol,nCC,sizeBuffer,'uint8');
    rainBufferY= zeros(mRow,mCol,sizeBuffer,'uint8');
    rainRemove= zeros(mRow,mCol,3,'uint8');
    rainCompensateY= zeros(mRow,mCol,'uint8');
    rainResidualY= zeros(mRow,mCol,'double');
    currRainCompensateY= zeros(mRow,mCol,'double');
    currSPCompensate= zeros(mRow,mCol,'double');
    
    rainCompensateY_Avg= zeros(mRow,mCol,'uint8');
    currRainCompensateY_Avg= zeros(mRow,mCol,'uint8');
    currSPCompensate_Avg= zeros(mRow,mCol,'uint8');
    
    rainCompensateY_Min= zeros(mRow,mCol,'uint8');
    currRainCompensateY_Min= zeros(mRow,mCol,'uint8');
    currSPCompensate_Min= zeros(mRow,mCol,'uint8');
    
    
    for nf=1:(sizeBuffer-1)
        newFrame= vidIn(:,:,:,nf);
        rainBuffer(:,:,:,nf+1)= rgb2ycbcr(newFrame);
        rainBufferY(:,:,nf+1)= squeeze(rainBuffer(:,:,1,nf+1));
    end
    
    rainCompensateY= rainBufferY(:,:,(sizeBuffer+1)/2);
    for nf=(sizeBuffer):numFrames
        
        %%  1. input new frame and manage buffer update
%         newFrame= im2uint8(imread([fileFolder,'/',pngList(nf).name]));
        newFrame= vidIn(:,:,:,nf);
        % update rain buffer
        rainBuffer(:,:,:,1:end-1)= rainBuffer(:,:,:,2:end);
        rainBuffer(:,:,:,end)= rgb2ycbcr(newFrame);
        rainBufferY(:,:,1:end-1)= rainBufferY(:,:,2:end);
        rainBufferY(:,:,end)= squeeze(rainBuffer(:,:,1,end));
        % update derain cache
        rainBufferY(:,:,(sizeBuffer-1)/2) =rainCompensateY; % udpate history buffer
        
        currFrame= rainBuffer(:,:,:,(sizeBuffer+1)/2);
        %% calculate UV edges for target frame
        [Ux,Uy]= gradient(im2double(currFrame(:,:,2)));
        [Vx,Vy]= gradient(im2double(currFrame(:,:,3)));
        currUV= mean(abs(Ux)+abs(Uy)+abs(Vx)+abs(Vy),3)>0.05; % figure;imshow(currUV);
        
        bufferIm= im2uint8(rainBufferY(:,:,referenceIdx(1:(sizeBuffer-1))));
        targetIm= im2uint8(rainBufferY(:,:,referenceIdx(sizeBuffer)));
        %% extra allocation
        [mRow,mCol,nCC]= size(targetIm);
        currRainMask= zeros(mRow,mCol);
        rainMask= zeros(mRow,mCol);
        
        %% SP segmetnation for current frame
        [spInfo.spIm, spInfo.numSP]= slicmex(im2uint8(targetIm),spInfo.NP,50);
        %{
    [Gx,Gy]= gradient(double(spInfo.spIm));
    SegBG= ((Gx.^2+Gy.^2)~=0);
    figure;imshow(SegBG+im2double(targetIm));
        %}
        
        npatchT0= 0;
        %% First round of matching, with SP mask and rain mask
        spMatchParams.useSpMask = 0;
        spMatchParams.useInputMask = 0;
        spMatchParams.f_splabel= int32(spInfo.spIm);
        spMatchParams.f_mask= uint8(ones(mRow,mCol));
        currMatch = spMatching_mine(targetIm, bufferIm, spMatchParams);
        for np= 1:spInfo.numSP
            spMask= spInfo.spIm==(np-1);
            spIdx= find(spMask);
            
            [rows,cols]= ind2sub([mRow,mCol],spIdx);
            rowStart= min(rows);
            rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
            colStart= min(cols);
            colEnd= min(mCol,colStart+spMatchParams.blkSize-1);
            % matching details for current SP
            spMatch= currMatch(np).matches;
            spOffSet= currMatch(np).offsetToSpBox;
            T0= zeros(spMatchParams.blkSize,spMatchParams.blkSize,sizeBuffer,'uint8');
            for nv=1:sizeBuffer
                currIdx= nv*10;
                r0= spMatch(3,currIdx)+spOffSet(1)+1;
                r1= min(mRow,r0+spMatchParams.blkSize-1);
                c0= spMatch(4,currIdx)+spOffSet(2)+1;
                c1= min(mCol,c0+spMatchParams.blkSize-1);
                T0(1:(r1-r0+1),1:(c1-c0+1),nv)= rainBufferY(r0:r1,c0:c1,referenceIdx(nv));
            end
            accMask= sum((repmat(T0(:,:,sizeBuffer),1,1,sizeBuffer)-T0)>0.005,3)>=3;
            
            currRainMask(rowStart:rowEnd, colStart:colEnd)= ...
                accMask(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));
            rainMask(spIdx)= currRainMask(spIdx); % figure;imshow(rainMask);
            
            %%
            %         if(length(find(rainMask(spIdx)))/length(spIdx)<0.2)
            %             rainMask(spIdx)= 0;
            %         end
            %%
            npatchT0= npatchT0+1;
            INT0(:,:,:,npatchT0)          = T0;
        end
        rainMask= rainMask.*(~currUV);
        
        %% second round of matching, with SP mask and rain mask
        spMatchParams.useSpMask = 0;
        spMatchParams.useInputMask = 1;
        spMatchParams.f_splabel= int32(spInfo.spIm);
        spMatchParams.f_mask= uint8(~rainMask);
        currMatch2 = spMatching_mine(targetIm, bufferIm, spMatchParams);
        currMatch3 = spMatching_mine(targetIm, rainBufferY(:,:,1:(sizeBuffer-1)/2), spMatchParams);
        for np= 1:spInfo.numSP
            spMask= double(spInfo.spIm==(np-1));
            spIdx= find(spMask);
            
            [rows,cols]= ind2sub([mRow,mCol],spIdx);
            rowStart= min(rows);
            rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
            colStart= min(cols);
            colEnd= min(mCol,colStart+spMatchParams.blkSize-1);
            % matching details for current SP
            spMatch= currMatch2(np).matches;
            spOffSet= currMatch2(np).offsetToSpBox;
            T1= zeros(spMatchParams.blkSize,spMatchParams.blkSize,numMatch,'uint8');
            CurrPatch= zeros(spMatchParams.blkSize,spMatchParams.blkSize,3,'uint8');
            RAINMASK= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'double');
            SPMASK= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'double');
            GTFRAME= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'uint8');
            [errorVal,errorIdx]= sort(spMatch(1,1:40),'ascend');
            % [~,errorIdx]= sort(spMatch(1,1:numMatch*(sizeBuffer-1)),'ascend');
            ne=0;
            for ni=1:numMatch
                if(errorVal(ni)<0.1)
                    ne=ne+1;
                end
                currIdx= errorIdx(ni);
                nv= ceil(currIdx/10);
                r0= spMatch(3,currIdx)+spOffSet(1)+1;
                r1= min(mRow,r0+spMatchParams.blkSize-1);
                c0= spMatch(4,currIdx)+spOffSet(2)+1;
                c1= min(mCol,c0+spMatchParams.blkSize-1);
                T1(1:(r1-r0+1),1:(c1-c0+1),ni)= rainBufferY(r0:r1,c0:c1,referenceIdx(nv));
            end
            
            if(ne<5)
                currSPCompensate_Avg= mean(T1(:,:,1:5),3);
            else
                currSPCompensate_Avg= mean(T1,3);
            end
            currRainCompensateY_Avg(rowStart:rowEnd, colStart:colEnd)= ...
                currSPCompensate_Avg(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));
            rainCompensateY_Avg(spIdx)= currRainCompensateY_Avg(spIdx); % figure;imshow(rainCompensateY_Avg);
    
            
%             %% this part is only for data preparation
%             % 0. prepare current patch
%             CurrPatch(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1),:)...
%                 = rainBuffer(rowStart:rowEnd, colStart:colEnd,:,sizeBuffer);
%             % 1. extract rain map
%             RAINMASK(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1))= ...
%                 rainMask(rowStart:rowEnd, colStart:colEnd);
%             % 2. extract SP mask
%             SPMASK(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1))= ...
%                 spMask(rowStart:rowEnd, colStart:colEnd);
%             
%             inT1SpMask=	padarray(im2single(T1).*repmat(im2single(SPMASK),1,1,10,1),...
%                 [param.mBorder,param.mBorder],'both');
%             inT0SpMask=	padarray(im2single(INT0(:,:,:,np)).*repmat(im2single(SPMASK),1,1,5,1),...
%                 [param.mBorder,param.mBorder],'both');
%             inDataSpMask= padarray(im2single(CurrPatch).*repmat(im2single(SPMASK),1,1,3,1),...
%                 [param.mBorder,param.mBorder],'both');
%             inRainMask=	padarray(im2single(RAINMASK),[param.mBorder,param.mBorder],'both');
%             inSpMaskEx=	padarray(im2single(SPMASK),[param.mBorder,param.mBorder],'both');
%             
%             if (param.useGPU)
%                 inT1SpMask      = gpuArray(inT1SpMask);
%                 inT0SpMask      = gpuArray(inT0SpMask);
%                 inDataSpMask      = gpuArray(inDataSpMask);
%                 inRainMask      = gpuArray(inRainMask);
%                 inSpMaskEx      = gpuArray(inSpMaskEx);
%             end
%             
%             % [dispRes] = EvaluateSystem(dispNet,inDpMSP,inConf,inCView,inCEdge,gtDispRes,true,false); % this one is for training
%             [dispRes] = EvaluateSystem(dispNet,inT1SpMask,inT0SpMask,inDataSpMask,inSpMaskEx,inRainMask,[],false,false);
%             currSPCompensate= double(gather(dispRes(end).x)); % figure;imshow(finalOut);
%             currRainCompensateY(rowStart:rowEnd, colStart:colEnd)= ...
%                 currSPCompensate(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));
%             rainResidualY(spIdx)= currRainCompensateY(spIdx); % figure;imshow(rainCompensateY);
        end
        
%         rainCompensateY= im2uint8(im2double(rainCompensateY_Avg)+ rainResidualY);
%         rainCompensateY= rainCompensateY.*uint8(rainCompensateY>3)+ ...
%             currFrame(:,:,1).*uint8(rainCompensateY<=3); % figure;imshow(rainCompensateY);
%         rainMaskUpdate1= ((currFrame(:,:,1)- rainCompensateY>3) | rainMask)& (~currUV);  % figure;imshow(rainMaskUpdate1);
%         rainRemove(:,:,1)= currFrame(:,:,1).*(1-uint8(rainMask)) +rainCompensateY.*uint8(rainMask); % figure;imshow(rainRemove(:,:,1));
%         rainRemove(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemove);
%         rainRemove= ycbcr2rgb(rainRemove);
%         
%         rainRemove_dir(:,:,1)= rainCompensateY.*uint8(rainCompensateY~=0) ...
%             +currFrame(:,:,1).*uint8(rainCompensateY==0);
%         rainRemove_dir(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemove);
%         rainRemove_dir= ycbcr2rgb(rainRemove_dir);
        
        rainRemoveAvg(:,:,1)= currFrame(:,:,1).*(1-uint8(rainMask))+ rainCompensateY_Avg.*uint8(rainMask);
        rainRemoveAvg(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemoveAvg);
        rainRemoveAvg= ycbcr2rgb(rainRemoveAvg);
        
        rainRemoveAvg1(:,:,1)= rainCompensateY_Avg;
        rainRemoveAvg1(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemoveAvg);
        rainRemoveAvg1= ycbcr2rgb(rainRemoveAvg1);
        
        % save output
        output.rainRemoveAvg1(:,:,:,nf) = rainRemoveAvg1;
        output.rainRemoveAvg(:,:,:,nf) = rainRemoveAvg;
        
        % CNN
%         output.rainRemove_dir(:,:,:,nf) = rainRemove_dir(:,:,:,nf);
%         output.rainRemove(:,:,:,nf) = rainRemove(:,:,:,nf); % rainRemove is rain mask altered on rainRemove_dir

    end
    
end




