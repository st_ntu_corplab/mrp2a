clearvars; clearvars -global; close all;
warning off;

addpath(genpath('required/'));

vl_setupnn();
InitSystemParam();

[dispNet] = LoadNetworks(true);

TrainSystem(dispNet);