clear;

dataFolder= '\\eee-mtl-wdclp\jchen5\Academic\Project\ExperimentData\RainRemoval\SPAC_Paper\expData\';

fileName= 'fast_01_VGA';
% load ground truth
gtPath= [dataFolder,fileName];
rainPath= [dataFolder,fileName,'_rain_1'];

gtList= dir([gtPath,'/*.png']);
% load rain frames
rainList= dir([rainPath,'/*.png']);

numFrames= length(gtList);

for n=1:numFrames
    
    currGT= rgb2gray(imread([gtPath,'/',gtList(n).name]));
    currRain= rgb2gray(imread([rainPath,'/',rainList(n).name]));
    
    rainMask= (currRain- currGT)>0.01;
    
end