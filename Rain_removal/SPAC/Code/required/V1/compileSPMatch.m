
% clear;

OCVRoot = 'C:\My_Software\OpenCV\opencv320\build\install\x64\vc14\';
IPath = ['-I',fullfile(OCVRoot,'..\..\include')];
LPath = fullfile(OCVRoot, 'lib');
lib1 = fullfile(LPath,'opencv_core320.lib');
lib2 = fullfile(LPath,'opencv_ximgproc320.lib');
lib3 = fullfile(LPath,'opencv_highgui320.lib');
lib4 = fullfile(LPath,'opencv_imgproc320.lib');
lib5 = fullfile(LPath,'opencv_highgui320.lib');


mex('spMatch_mex.cpp', IPath, lib1, lib2, lib3, lib4, lib5);

%% testing
% Im= im2uint8(imread('Truck.png'));
% 
% [spIm, numSP] = prepMSLIC(size(Im,1),size(Im,2),Im,100,50,10);
% 
% [Gx,Gy]= gradient(double(spIm));
% SegBG= ((Gx.^2+Gy.^2)~=0);
% figure;imshow(repmat(~SegBG,1,1,3).*im2double(Im));