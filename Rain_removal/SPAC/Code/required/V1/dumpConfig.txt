batchFrames = 0
imageEnhance = 0
useGPU = 0

[Derain]
TH_rain = 3
edgeUvRejectThreshold = 0.05
slicNumSP = 288
slicWeight = 30
useRgbForSpMatching = 1

[Motion]
motionModel = 7

[SpMatching]
TH_fractionGoodSpPix = 0.2
TH_spMatch1_step1 = 510
TH_spMatch2 = 0.01
TH_spMatch2_step1 = 0.01
spSearchRange_0x = 100
spSearchRange_0y = 30
spSearchRange_1x = 0
spSearchRange_1y = 0
spTemplateUpsizeFactor = 1.5
