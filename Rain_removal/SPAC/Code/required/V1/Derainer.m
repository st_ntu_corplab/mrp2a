classdef Derainer < handle
    
    properties (SetAccess = private)
        % Object ID
        id
    end

    methods
        function this = Derainer(height, width)
            this.id = Derainer_(0, 'new', height, width);
        end

%         function delete(this)
%             %DELETE  Destructor
%             %
%             %    vid.delete()
%             %
%             % The method first calls cv.VideoWriter.release to close the
%             % already opened file.
%             %
%             % See also: cv.VideoWriter.release
%             %
%             if isempty(this.id), return; end
%             Derainer_(this.id, 'delete');
%         end

        % retval is an array of struct, where each element of the array corresponds to a superpixel
        % tlbr - top left (y,x), bottom right (y,x) of SP bounding box
        % SPMask - indicates SP pixels in bounding box. SP-255, ~SP-0
        % rainMask - indicates rain pixels. Rain-255, nonRain-0
        % matches - Array of reference SPs for each SP. 10 closest matching SPs retrieved per frame
        %         - row1 - matching error (MSE/spPixel)
        %         - row2 - frame index of reference SP. Processed SP is at frame 2. First frame = frame 4
        %         - row3,4 - y,x coordinate of reference SP
        function retval = step(this, frame)
            retval = Derainer_(this.id, 'step', frame);
        end
    end

end
