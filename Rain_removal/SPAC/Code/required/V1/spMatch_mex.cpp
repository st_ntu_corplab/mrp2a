
#include <iostream>
#include "opencv2\core\core.hpp"
#include <opencv2\highgui\highgui.hpp>
#include "opencv2\imgproc.hpp"
#include "opencv2\opencv.hpp"
#include "opencv2\ximgproc\slic.hpp"

#include "mex.h"

using namespace cv;

#ifndef HAS_OPENCV
#define HAS_OPENCV
#endif

// reading from video
//int main(){
//String filePath = "D:/Dropbox/Project/TestProgramming/OpenCV_Gerneral/vidProc/Resource/NYC1.jpg";
//	String filePath = "D:/Dropbox/Project/Flow/GlobalPatchCollider/GPCRUN/RC/Cup_1_01__Decoded_Thumb.png";
//Mat img = imread(filePath);
//IplImage* img = cvLoadImage(filePath); //change the name (image.jpg) according to your Image filename.
//cvNamedWindow( "Example1", CV_WINDOW_NORMAL );
//cvShowImage("Example1", img);
//cvWaitKey(0);
//cvReleaseImage( &img );
//cvDestroyWindow( "Example1" );

//return 0;
//}

/* Input Arguments */

#define	im_in			prhs[0]  // spatial image width
#define	ref_in			prhs[1]  // spatial image height
#define	rowSize_in		prhs[2]  // input target frame
#define	colSize_in		prhs[3]  // reference frame buffer for sp matching
#define	bufferSize_in	prhs[4]  // reference frame buffer depth

/* Output Arguments */
#define matchOut			plhs[0] // output structure

/* Variables */
int mRow;
int mCol;
int bufferSize;
int pixelNum;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) 
{
	/* Check for proper number of arguments */
	if (nrhs != 5)
		mexErrMsgTxt("Three input arguments required.");
	else if (nlhs != 1)
		mexErrMsgTxt("Wrong output arguments.");

	/* Assign pointers to the various parameters */
	double* mRow_Pt	= mxGetPr(rowSize_in);
	double*	mCol_Pt	= mxGetPr(colSize_in);
	double*	bSize_Pt = mxGetPr(bufferSize_in);
	char*	ImIn_pt = (char*)mxGetPr(im_in);
	char*	RefIn_pt = (char*)mxGetPr(ref_in);
	
	// Assign parameters
	mRow = (int)*mRow_Pt;
	mCol = (int)*mCol_Pt;
	bufferSize = (int)*bSize_Pt;
	pixelNum =	mRow*mCol;

	/* Create the output matrix*/
	const mwSize dims2[] = {4, 10, bufferSize};
	matchOut= mxCreateNumericArray(3, dims2, mxDOUBLE_CLASS, mxREAL);
	double* matchOut_pt = (double*)mxGetPr(matchOut);

	// Assign input matrices
	Mat currFrame = Mat::zeros(mRow, mCol, CV_8UC3);
	int dims[3] = {mRow,mCol,bufferSize};
	//Mat m(3, dims, CV_32F);
	Mat bufferIm(3, dims, CV_8UC3);

	for (int x = 0; x < mCol; x++) {
		for (int y = 0; y < mRow; y++) {		
			for (int c = 0; c < 3; c++) {
				currFrame.at<Vec3b>(y, x)[2 - c] = ImIn_pt[y + x*mRow + c*pixelNum];
			}
		}
	}
	
	for (int x = 0; x < mCol; x++) {
		for (int y = 0; y < mRow; y++) {
			for (int c = 0; c < 3; c++) {
				for (int z = 0; z < bufferSize; z++) {
					bufferIm.at<Vec3b>(y, x, z)[2 - c] = 0;//RefIn_pt[y + x*mRow + c*pixelNum + z*pixelNum*3];
				}
			}
		}
	}
	

	//double minVal, maxVal;
	//cv::minMaxLoc(spIm, &minVal, &maxVal);
	//*numSP_out_pt = maxVal + 1;

	cv::namedWindow("ShowImage", CV_WINDOW_AUTOSIZE);
	cv::imshow("ShowImage",bufferIm.at<Vec3b>(y, x, z));
	cv::waitKey(0);

}
