Requires DLL library. All DLL files can be found in .\_STEng\src\Codelib\
	opencv 3.2 compiled with cuda (.\opencv-3.2-cuda)
	Cuda dll (.\CUDA)
	Intel thread building blocks (.\tbb)
	
	
% function
% output is an array of struct, where each element of the array corresponds to a superpixel
% tlbr - top left (y,x), bottom right (y,x) of SP bounding box
% SPMask - indicates SP pixels in bounding box. SP-255, ~SP-0
% rainMask - indicates rain pixels in bounding box. rain-255, ~rain-0
% matches - Array of reference SPs for each SP. 10 closest matching SPs retrieved per frame
%         - row1 - matching error (MSE/spPixel)
%         - row2 - frame index of reference SP. Processed SP is at frame 2. First frame = frame 4
%         - row3,4 - y,x coordinate of reference SP

output = derainer.step(input frame)

