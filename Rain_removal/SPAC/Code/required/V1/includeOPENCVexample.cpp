
#include <iostream>
#include "opencv2\core\core.hpp"
#include <opencv2\highgui\highgui.hpp>
#include "opencv2\imgproc.hpp"
#include "opencv2\opencv.hpp"
#include "opencv2\ximgproc\slic.hpp"

#include "mex.h"

using namespace cv;

#ifndef HAS_OPENCV
#define HAS_OPENCV
#endif

// reading from video
//int main(){
//String filePath = "D:/Dropbox/Project/TestProgramming/OpenCV_Gerneral/vidProc/Resource/NYC1.jpg";
//	String filePath = "D:/Dropbox/Project/Flow/GlobalPatchCollider/GPCRUN/RC/Cup_1_01__Decoded_Thumb.png";
//Mat img = imread(filePath);
//IplImage* img = cvLoadImage(filePath); //change the name (image.jpg) according to your Image filename.
//cvNamedWindow( "Example1", CV_WINDOW_NORMAL );
//cvShowImage("Example1", img);
//cvWaitKey(0);
//cvReleaseImage( &img );
//cvDestroyWindow( "Example1" );

//return 0;
//}

/* Input Arguments */

#define	rowSize_in			prhs[0]  // spatial image width
#define	colSize_in			prhs[1]  // spatial image height
#define	im_in				prhs[2]  // input image for segmentation, must be color image
#define	SP_span_in				prhs[3]  // input image for segmentation, must be color image
#define connectivity_in			prhs[4]
#define	interNum_in				prhs[5]  // input image for segmentation, must be color image

/* Output Arguments */
#define spIm_out			plhs[0]  // correspondence cost
#define numSP_out			plhs[1]  // correspondence cost
/* Variables */
int mRow;
int mCol;
int pixelNum;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) 
{
	/* Check for proper number of arguments */
	if (nrhs != 6)
		mexErrMsgTxt("Three input arguments required.");
	else if (nlhs != 2)
		mexErrMsgTxt("Wrong output arguments.");


	/* Assign pointers to the various parameters */
	double* mRow_Pt	= mxGetPr(rowSize_in);
	double*	mCol_Pt	= mxGetPr(colSize_in);
	char* ImIn_pt = (char*)mxGetPr(im_in);
	double* sp_span_pt = mxGetPr(SP_span_in);
	double* connectivity_pt = mxGetPr(connectivity_in);
	double* interNum_pt = mxGetPr(interNum_in);
	
	// Assign parameters
	mRow		=	(int) *mRow_Pt;
	mCol		=   (int) *mCol_Pt;
	pixelNum	=	mRow*mCol;

	/* Create the output matrix */
	const mwSize dims2[] = { mRow, mCol};
	spIm_out= mxCreateNumericArray(2, dims2, mxDOUBLE_CLASS, mxREAL);
	double* spIm_out_pt = (double*)mxGetPr(spIm_out);

	const mwSize dims1[] = {1,1};
	numSP_out = mxCreateNumericArray(2, dims1, mxDOUBLE_CLASS, mxREAL);
	double* numSP_out_pt = (double*)mxGetPr(numSP_out);

	// Assign matrices
	//refocus = new float[pixelNum*3];
	Mat ImIn = Mat::zeros(mRow, mCol, CV_8UC3);
	for (int x = 0; x < mCol; x++) {
		for (int y = 0; y < mRow; y++) {		
			for (int c = 0; c < 3; c++) {
				ImIn.at<Vec3b>(y, x)[2 - c] = ImIn_pt[y + x*mRow + c*pixelNum];
			}
		}
	}

	// Extract superpixels. SEEDS algorithm should be faster than SLIC
	int SP_span = (int) *sp_span_pt;
	int connectivity = (int)*connectivity_pt;
	int slicNumIterations = (int) *interNum_pt;
	Mat spIm = Mat::zeros(mRow, mCol, CV_32SC1);

	cv::Ptr<cv::ximgproc::SuperpixelSLIC> segmenter =
		cv::ximgproc::createSuperpixelSLIC(ImIn, cv::ximgproc::MSLIC, SP_span);
		//cv::ximgproc::createSuperpixelSLIC(ImIn, cv::ximgproc::SLICO, SP_span);

	segmenter->iterate(slicNumIterations);
	segmenter->enforceLabelConnectivity(connectivity);
	segmenter->getLabels(spIm); // f_label is WxH integer array. Each pixel's value denotes its cluster

	for (int y = 0; y < mRow; y++) {
		for (int x = 0; x < mCol; x++) {
				spIm_out_pt[y + x*mRow] = spIm.at<int>(y, x);
		}
	}


	double minVal, maxVal;
	cv::minMaxLoc(spIm, &minVal, &maxVal);
	*numSP_out_pt = maxVal + 1;

	//cv::namedWindow("ShowImage", CV_WINDOW_AUTOSIZE);
	//cv::imshow("ShowImage", spIm);
	//cv::waitKey(0);

}
