clear all

vid = vidread('\\eee-mtl-wdclp\Share folder\_STEng\RainData\STEng\08JUN_rainDrive\CanB_downslope_1\CanB_downslope_1.avi');

[h,w,~,~] = size(vid);

derainer = Derainer(h,w);

for i=1:5
    output = derainer.step(vid(:,:,:,i));
end
