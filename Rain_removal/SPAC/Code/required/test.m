% clear all

vid = vidread('D:\Dropbox\Work\Rain_removal\Database\rain\STEng\CanB_downslope_1.avi');

im = vid(:,:,:,1);

imBuf = {vid(:,:,:,2), vid(:,:,:,3),vid(:,:,:,4),vid(:,:,:,5)};

[height,width,~] = size(im);

[L,nSP] = superpixels(im,50);
f_splabel = int32(L);


f_mask = uint8(ones(height,width));

% f_splabel; % int32 image
% f_mask; % uint8 image
% nSP,search_y,search_x; % double

% nSP = 100;
search_y = 10;
search_x = 10;
useSpMask = 0;
useInputMask = 0;

output = spMatching(im, imBuf, f_splabel, f_mask, search_y, search_x, useSpMask, useInputMask);

clear spMatching_mex