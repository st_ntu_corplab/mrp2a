% function error = TestDuringTraining(dispNet)
clearvars; clearvars -global; close all;
warning off;

addpath(genpath('required/'));
% vl_setupnn();
InitSystemParam();
global param;

param.testNet= 'testNet\';
netList= dir([param.testNet,'*.mat']);
% netNo= 10;
load([param.testNet,netList(end).name]);

sourceCode= 1; % 0: video; 1: PNG sequence
outputFlag= 0; % 0: only display result 1: write output
if(sourceCode==0)
    filePath= '\\eee-mtl-wdclp\Share folder\_STEng\RainData\STEng\08JUN_rainDrive\CanB_downslope_1\CanB_downslope_1.avi';
    vid = vidread(filePath);
    [mRow,mCol,nCC,numFrames] = size(vid);
elseif(sourceCode==1)
% 	contentFolder= '\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Rain\_Simulation\_testing\';
	contentFolder= 'D:\Dropbox\Work\Rain_removal\results\temptest\baseline\CanB_downslope_1\';
% 	contentFolder= '\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Rain\rainDriveSet_01\';
% 	contentFolder= '\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Rain\_SpacTesting\';    
end    

% choose sequence
folderList= dir(contentFolder);
for nf=1:length(folderList)
    fprintf(['%d. ',folderList(nf).name,'\n'],nf);
end 
numF= input('Please chose rain sequence: ');   

for nn= 1:length(numF)
vecPSNR= [];    
fileFolder= [contentFolder,folderList(numF(nn)).name,'\'];
if(outputFlag==1)
    outputFolder= ['\\eee-mtl-wdclp\jchen5\Academic\Project\ExperimentData\RainRemoval\SPAC_Paper\SPAC-CNN\',...
            folderList(numF(nn)).name,'_removal\'];
  	outputFolder2= ['\\eee-mtl-wdclp\jchen5\Academic\Project\ExperimentData\RainRemoval\SPAC_Paper\SPAC-CNN-masked\',...
            folderList(numF(nn)).name,'_removal\'];
    if(~isdir(outputFolder))
        mkdir(outputFolder);
    end
    if(~isdir(outputFolder2))
        mkdir(outputFolder2);
	end
end
pngList= dir([fileFolder,'*.png']);   
numFrames= length(pngList);    
[mRow,mCol,nCC]= size(imread([fileFolder,pngList(1).name]));

strLoc= strfind(folderList(numF(nn)).name,'_');
gtFolder= dir([contentFolder,'\..\GT_',folderList(numF(nn)).name(strLoc(1)+1:strLoc(end)-1),'*']);
if(length(gtFolder))
    gtFlag=1;
    gtFolder= ['\..\',gtFolder(1).name,'\'];
else
    gtFlag=0;
end
    
sizeBuffer= 5;
rainBuffer= zeros(mRow,mCol,nCC,sizeBuffer,'uint8');
rainBufferY= zeros(mRow,mCol,sizeBuffer,'uint8');
rainRemove= zeros(mRow,mCol,3,'uint8');
rainCompensateY= zeros(mRow,mCol,'uint8');
rainResidualY= zeros(mRow,mCol,'double');
currRainCompensateY= zeros(mRow,mCol,'double');
currSPCompensate= zeros(mRow,mCol,'double');

rainCompensateY_Avg= zeros(mRow,mCol,'uint8');
currRainCompensateY_Avg= zeros(mRow,mCol,'uint8');
currSPCompensate_Avg= zeros(mRow,mCol,'uint8');

rainCompensateY_Min= zeros(mRow,mCol,'uint8');
currRainCompensateY_Min= zeros(mRow,mCol,'uint8');
currSPCompensate_Min= zeros(mRow,mCol,'uint8');

referenceIdx= [1,2,4,5,3];
numMatch= 10;

% SP ST params
spInfo.NP= 288; 
spMatchParams.blkSize= 80; 
% spInfo.NK= round(mRow*mCol/spInfo.NP); round(2.5*sqrt(spInfo.NK));
spMatchParams.search_y= 30;
spMatchParams.search_x= 30;

startFrame= 1;
% for ni= startFrame:(startFrame+ sizeBuffer-2)
for nf=1:(sizeBuffer-1)
    newFrame= im2uint8(imread([fileFolder,'/',pngList(startFrame+nf-1).name]));
    rainBuffer(:,:,:,nf+1)= rgb2ycbcr(newFrame);
    rainBufferY(:,:,nf+1)= squeeze(rainBuffer(:,:,1,nf+1));
end

% rainCompensateY_Avg= rainBufferY(:,:,(sizeBuffer+1)/2);
rainCompensateY= rainBufferY(:,:,(sizeBuffer+1)/2);
showCount= 0;
for nf=(startFrame+ sizeBuffer-1):numFrames
        
    %%  1. input new frame and manage buffer update
    newFrame= im2uint8(imread([fileFolder,'/',pngList(nf).name]));
    % update rain buffer
    rainBuffer(:,:,:,1:end-1)= rainBuffer(:,:,:,2:end);
    rainBuffer(:,:,:,end)= rgb2ycbcr(newFrame);
    rainBufferY(:,:,1:end-1)= rainBufferY(:,:,2:end);
    rainBufferY(:,:,end)= squeeze(rainBuffer(:,:,1,end));
     % update derain cache
    rainBufferY(:,:,(sizeBuffer-1)/2) =rainCompensateY; % udpate history buffer
    
    currFrame= rainBuffer(:,:,:,(sizeBuffer+1)/2);
	%% calculate UV edges for target frame
    [Ux,Uy]= gradient(im2double(currFrame(:,:,2)));
    [Vx,Vy]= gradient(im2double(currFrame(:,:,3)));
    currUV= mean(abs(Ux)+abs(Uy)+abs(Vx)+abs(Vy),3)>0.05; % figure;imshow(currUV);    
    
    bufferIm= im2uint8(rainBufferY(:,:,referenceIdx(1:(sizeBuffer-1))));
    targetIm= im2uint8(rainBufferY(:,:,referenceIdx(sizeBuffer)));
    %% extra allocation
    [mRow,mCol,nCC]= size(targetIm);
    currRainMask= zeros(mRow,mCol);
    rainMask= zeros(mRow,mCol);
    
    %% SP segmetnation for current frame
    [spInfo.spIm, spInfo.numSP]= slicmex(im2uint8(targetIm),spInfo.NP,50);
    %{
    [Gx,Gy]= gradient(double(spInfo.spIm));
    SegBG= ((Gx.^2+Gy.^2)~=0);
    figure;imshow(SegBG+im2double(targetIm));
    %}

    npatchT0= 0;
    %% First round of matching, with SP mask and rain mask
    spMatchParams.useSpMask = 0;
    spMatchParams.useInputMask = 0;
    spMatchParams.f_splabel= int32(spInfo.spIm);
    spMatchParams.f_mask= uint8(ones(mRow,mCol));    
    currMatch = spMatching_mine(targetIm, bufferIm, spMatchParams);   
    for np= 1:spInfo.numSP     
        spMask= spInfo.spIm==(np-1); 
        spIdx= find(spMask);

        [rows,cols]= ind2sub([mRow,mCol],spIdx);       
        rowStart= min(rows);
        rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
        colStart= min(cols);
        colEnd= min(mCol,colStart+spMatchParams.blkSize-1); 
        % matching details for current SP
        spMatch= currMatch(np).matches;  
        spOffSet= currMatch(np).offsetToSpBox;
        T0= zeros(spMatchParams.blkSize,spMatchParams.blkSize,sizeBuffer,'uint8');
        for nv=1:sizeBuffer
            currIdx= nv*10;
            r0= spMatch(3,currIdx)+spOffSet(1)+1;
            r1= min(mRow,r0+spMatchParams.blkSize-1);
            c0= spMatch(4,currIdx)+spOffSet(2)+1;
            c1= min(mCol,c0+spMatchParams.blkSize-1);           
            T0(1:(r1-r0+1),1:(c1-c0+1),nv)= rainBufferY(r0:r1,c0:c1,referenceIdx(nv));
        end
        accMask= sum((repmat(T0(:,:,sizeBuffer),1,1,sizeBuffer)-T0)>0.005,3)>=3; 

        currRainMask(rowStart:rowEnd, colStart:colEnd)= ...
                accMask(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));    
        rainMask(spIdx)= currRainMask(spIdx); % figure;imshow(rainMask);
        
        %%
%         if(length(find(rainMask(spIdx)))/length(spIdx)<0.2)
%             rainMask(spIdx)= 0;
%         end
        %%
        npatchT0= npatchT0+1;      
      	INT0(:,:,:,npatchT0)          = T0;
    end
    rainMask= rainMask.*(~currUV);

    %% second round of matching, with SP mask and rain mask
    spMatchParams.useSpMask = 0;
    spMatchParams.useInputMask = 1;
    spMatchParams.f_splabel= int32(spInfo.spIm);
    spMatchParams.f_mask= uint8(~rainMask);    
    currMatch2 = spMatching_mine(targetIm, bufferIm, spMatchParams); 
    currMatch3 = spMatching_mine(targetIm, rainBufferY(:,:,1:(sizeBuffer-1)/2), spMatchParams);  
    for np= 1:spInfo.numSP     
        spMask= double(spInfo.spIm==(np-1));
        spIdx= find(spMask);

        [rows,cols]= ind2sub([mRow,mCol],spIdx);       
        rowStart= min(rows);
        rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
        colStart= min(cols);
        colEnd= min(mCol,colStart+spMatchParams.blkSize-1); 
        % matching details for current SP
        spMatch= currMatch2(np).matches;  
        spOffSet= currMatch2(np).offsetToSpBox;                
        T1= zeros(spMatchParams.blkSize,spMatchParams.blkSize,numMatch,'uint8'); 
        CurrPatch= zeros(spMatchParams.blkSize,spMatchParams.blkSize,3,'uint8'); 
        RAINMASK= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'double');
        SPMASK= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'double');
        GTFRAME= zeros(spMatchParams.blkSize,spMatchParams.blkSize,'uint8');
        [errorVal,errorIdx]= sort(spMatch(1,1:40),'ascend');
        % [~,errorIdx]= sort(spMatch(1,1:numMatch*(sizeBuffer-1)),'ascend');
        ne=0;
        for ni=1:numMatch
            if(errorVal(ni)<0.1)
                ne=ne+1;
            end
            currIdx= errorIdx(ni);
            nv= ceil(currIdx/10);
            r0= spMatch(3,currIdx)+spOffSet(1)+1;
            r1= min(mRow,r0+spMatchParams.blkSize-1);
            c0= spMatch(4,currIdx)+spOffSet(2)+1;
            c1= min(mCol,c0+spMatchParams.blkSize-1);                        
            T1(1:(r1-r0+1),1:(c1-c0+1),ni)= rainBufferY(r0:r1,c0:c1,referenceIdx(nv));  
        end
        
        if(ne<5)
           	currSPCompensate_Avg= mean(T1(:,:,1:5),3);
        else
            currSPCompensate_Avg= mean(T1,3);            
        end
        currRainCompensateY_Avg(rowStart:rowEnd, colStart:colEnd)= ...
                currSPCompensate_Avg(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));    
        rainCompensateY_Avg(spIdx)= currRainCompensateY_Avg(spIdx); % figure;imshow(rainCompensateY_Avg);
        
        % currSPCompensate_Min= min(T1,[],3);
        % currRainCompensateY_Min(rowStart:rowEnd, colStart:colEnd)= ...
        %         currSPCompensate_Min(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1)); 
        % rainCompensateY_Min(spIdx)= currRainCompensateY_Min(spIdx); % figure;imshow(rainCompensateY_Avg);

        %% this part is only for data preparation
        % 0. prepare current patch
        CurrPatch(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1),:)...
                = rainBuffer(rowStart:rowEnd, colStart:colEnd,:,sizeBuffer);
        % 1. extract rain map
        RAINMASK(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1))= ...
                            rainMask(rowStart:rowEnd, colStart:colEnd);
        % 2. extract SP mask
        SPMASK(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1))= ...
                            spMask(rowStart:rowEnd, colStart:colEnd);
        % 3. read ground truth patch
        % gtCurrFrame= im2uint8(imread([videoFolder,gtFileName,'\',...
        %                     frameList(nf+referenceIdx(sizeBuffer)-1).name]));
        % gtCurrFrameY= rgb2ycbcr(gtCurrFrame);
        % gtCurrFrameY= gtCurrFrameY(:,:,1);
        % GTFRAME(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1))= ...
        %             gtCurrFrameY(rowStart:rowEnd, colStart:colEnd);          
        % imshow([showLFIM(T1)/255,GTFRAME/255]);    
        % npatch= npatch+1;           
        % INT1(:,:,:,npatch)          = T1; 
        % INRAINMASK(:,:,1,npatch)	= im2uint8(RAINMASK);
        % INSPMASK(:,:,1,npatch)      = im2uint8(SPMASK);
        % GTIM(:,:,1,npatch)          = GTFRAME;

        % [errorVal,errorIdx]= sort(spMatch(1,41:50),'ascend');
        inT1SpMask=	padarray(im2single(T1).*repmat(im2single(SPMASK),1,1,10,1),...
                            [param.mBorder,param.mBorder],'both');
     	inT0SpMask=	padarray(im2single(INT0(:,:,:,np)).*repmat(im2single(SPMASK),1,1,5,1),...
                            [param.mBorder,param.mBorder],'both');
       	inDataSpMask= padarray(im2single(CurrPatch).*repmat(im2single(SPMASK),1,1,3,1),...
                            [param.mBorder,param.mBorder],'both');
        inRainMask=	padarray(im2single(RAINMASK),[param.mBorder,param.mBorder],'both'); 
        inSpMaskEx=	padarray(im2single(SPMASK),[param.mBorder,param.mBorder],'both');
        
        if (param.useGPU)
            inT1SpMask      = gpuArray(inT1SpMask);
            inT0SpMask      = gpuArray(inT0SpMask);
            inDataSpMask      = gpuArray(inDataSpMask);
            inRainMask      = gpuArray(inRainMask);
            inSpMaskEx      = gpuArray(inSpMaskEx);
        end

        % [dispRes] = EvaluateSystem(dispNet,inDpMSP,inConf,inCView,inCEdge,gtDispRes,true,false); % this one is for training
        [dispRes] = EvaluateSystem(dispNet,inT1SpMask,inT0SpMask,inDataSpMask,inSpMaskEx,inRainMask,[],false,false);
        % currSPCompensate= im2uint8(gather(dispRes(end).x)); % figure;imshow(finalOut);
        currSPCompensate= double(gather(dispRes(end).x)); % figure;imshow(finalOut);
        % showLFIM(T1);
        currRainCompensateY(rowStart:rowEnd, colStart:colEnd)= ...
                currSPCompensate(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));    
%         rainCompensateY(spIdx)= currRainCompensateY(spIdx); % figure;imshow(rainCompensateY); 
        rainResidualY(spIdx)= currRainCompensateY(spIdx); % figure;imshow(rainCompensateY); 
        % average of T1 for comparison
    end
    
%     temp= rainCompensateY_Avg.*uint8(rainMask)+ currFrame(:,:,1).*(1-uint8(rainMask));
    rainCompensateY= im2uint8(im2double(rainCompensateY_Avg)+ rainResidualY);
    rainCompensateY= rainCompensateY.*uint8(rainCompensateY>3)+ ...
                       currFrame(:,:,1).*uint8(rainCompensateY<=3); % figure;imshow(rainCompensateY);
    rainMaskUpdate1= ((currFrame(:,:,1)- rainCompensateY>3) | rainMask)& (~currUV);  % figure;imshow(rainMaskUpdate1);  
    rainRemove(:,:,1)= currFrame(:,:,1).*(1-uint8(rainMask)) +rainCompensateY.*uint8(rainMask); % figure;imshow(rainRemove(:,:,1));
    % rainRemove(:,:,1)= rainCompensateY;
	rainRemove(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemove);
	rainRemove= ycbcr2rgb(rainRemove);
    
    rainRemove_dir(:,:,1)= rainCompensateY.*uint8(rainCompensateY~=0) ...
                    +currFrame(:,:,1).*uint8(rainCompensateY==0);
    rainRemove_dir(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemove);
    rainRemove_dir= ycbcr2rgb(rainRemove_dir);
    
    rainRemoveAvg(:,:,1)= currFrame(:,:,1).*(1-uint8(rainMask))+ rainCompensateY_Avg.*uint8(rainMask);
    % rainRemoveAvg(:,:,1)= rainCompensateY_Avg;
    rainRemoveAvg(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemoveAvg);
    rainRemoveAvg= ycbcr2rgb(rainRemoveAvg);
    
    rainRemoveAvg1(:,:,1)= rainCompensateY_Avg;
    % rainRemoveAvg(:,:,1)= rainCompensateY_Avg;
    rainRemoveAvg1(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemoveAvg);
    rainRemoveAvg1= ycbcr2rgb(rainRemoveAvg1);
    
    % read ground truth and cal PSNR.
    if(outputFlag~=1)
        if(gtFlag)
            gtCurrFrame= im2uint8(imread([contentFolder,gtFolder,pngList(nf- (sizeBuffer-1)/2).name]));
            fprintf(repmat('\b',[1,showCount]));
            showCount= fprintf('Frame %d \tRain: %.2f\tAvg: %.2f/%.2f\tCNN: %.2f/%.2f\n',...
                    nf- (sizeBuffer-1)/2, calPSNR(gtCurrFrame,ycbcr2rgb(currFrame)), ...
                    calPSNR(gtCurrFrame,rainRemoveAvg1), ...
                    calPSNR(gtCurrFrame,rainRemoveAvg),...
                    calPSNR(gtCurrFrame,rainRemove_dir),... % our algorithm output
                    calPSNR(gtCurrFrame,rainRemove)); % rainRemove is rain mask altered on rainRemove_dir
            errorCompMask= mean(abs(rainRemove-gtCurrFrame),3)>mean(abs(rainRemoveAvg-gtCurrFrame),3);
        end
        imshow([ycbcr2rgb(currFrame),repmat(im2uint8(rainMask),1,1,3),rainRemove]);
        imshow(permute([permute([ycbcr2rgb(currFrame),rainRemoveAvg,rainRemove],[2,1,3]),...
            permute([repmat([im2uint8(rainMask),rainCompensateY_Avg],1,1,3),rainRemove_dir],[2,1,3])],[2,1,3]));
%         im2uint8(errorCompMask)
        vecPSNR(nf- (sizeBuffer-1)/2,1)= calPSNR(gtCurrFrame,rainRemoveAvg);
    elseif(outputFlag==1)
        fprintf(repmat('\b',[1,showCount]));
        showCount= fprintf('Finished frame %d/%d\n',nf- (sizeBuffer-1)/2,numFrames);
        imwrite(rainRemove_dir,[outputFolder,sprintf('%.5d.png',nf- (sizeBuffer-1)/2)],'png'); 
        % imwrite(rainRemove,[outputFolder2,sprintf('%.5d.png',nf- (sizeBuffer-1)/2)],'png');
    end
    
end

% save([outputFolder,'F1.mat'],'vecPSNR');

end




% negMask= uint8((rainCompensateY-currFrame(:,:,1))>3);
% figure;imshow(negMask.*currFrame(:,:,1)+ (1-negMask).*rainCompensateY);
% 
% figure;imshow(negMask.*currFrame(:,:,1)+ (1-negMask).*rainCompensateY_Avg);



