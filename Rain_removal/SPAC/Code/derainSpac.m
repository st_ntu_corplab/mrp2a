clear;
warning off;

addpath(genpath('required/'));

sourceCode= 0; % 0: video; 1: PNG sequence
if(sourceCode==0)
    filePath= '\\eee-mtl-wdclp\Share folder\_STEng\RainData\STEng\08JUN_rainDrive\CanB_downslope_1\CanB_downslope_1.avi';
    vid = vidread(filePath);
    [mRow,mCol,numFrames] = size(vid);
    nCC=3;
elseif(sourceCode==1)
    fileFolder= ['\\eee-mtl-wdclp\jchen5\Academic\Project\ExperimentData\RainRemoval\SPAC_Paper\expData\',...
                'fast_01_VGA_rain_5\'];
%     fileFolder= '\\eee-mtl-wdclp\jchen5\Academic\Project\_Resource\Rain\STEng\08JUN_rainDrive\VGA\Innovation_1\RainFrames\';
    pngList= dir([fileFolder,'/*.png']);   
    numFrames= length(pngList);    
    [mRow,mCol,nCC]= size(imread([fileFolder,'/',pngList(1).name]));
end   
% derainer = Derainer(mRow,mCol);

sizeBuffer= 5;
rainBuffer= zeros(mRow,mCol,nCC,sizeBuffer);
rainBufferY= zeros(mRow,mCol,sizeBuffer);
rainMask= zeros(mRow,mCol);
currRainMask= zeros(mRow,mCol);
rainRemove= zeros(mRow,mCol,3);
rainCompensateY= zeros(mRow,mCol);
currRainCompensateY= zeros(mRow,mCol);
rainCompensate= zeros(mRow,mCol);
currSPCompensate= zeros(mRow,mCol);
numMatch= 10;

% SP pareamters
spInfo.NP= 288; 

% SP ST params
spMatchParams.blkSize= 80; 
% spInfo.NK= round(mRow*mCol/spInfo.NP); round(2.5*sqrt(spInfo.NK));
spMatchParams.search_y= 30;
spMatchParams.search_x= 30;

for ni= 1:(sizeBuffer-1)
    newFrame= im2double(imread([fileFolder,'/',pngList(ni).name]));
    rainBuffer(:,:,:,ni+1)= rgb2ycbcr(newFrame);
    rainBufferY(:,:,ni+1)= squeeze(rainBuffer(:,:,1,ni+1));
end

for ni=sizeBuffer:numFrames
    
    %%  1. input new frame and manage buffer update
    newFrame= im2double(imread([fileFolder,'/',pngList(ni).name]));
    % update buffer
    rainBuffer(:,:,:,1:end-1)= rainBuffer(:,:,:,2:end);
    rainBuffer(:,:,:,end)= rgb2ycbcr(newFrame);
    rainBufferY(:,:,1:end-1)= rainBufferY(:,:,2:end);
    rainBufferY(:,:,end)= squeeze(rainBuffer(:,:,1,end));
    
    currFrame= rainBuffer(:,:,:,(sizeBuffer+1)/2);
    currFrameY= rainBufferY(:,:,(sizeBuffer+1)/2);
    % uvEdge= (edge(currFrameHSV(:,:,2),0.05));
    [Ux,Uy]= gradient(currFrame(:,:,2));
    [Vx,Vy]= gradient(currFrame(:,:,3));
    currUV= mean(abs(Ux)+abs(Uy)+abs(Vx)+abs(Vy),3)>0.01; % figure;imshow(currUV);
    
    %% 2. SP segmetnation for current frame
    [spInfo.spIm, spInfo.numSP]= slicmex(im2uint8(currFrameY),spInfo.NP,30);
    %{
    [Gx,Gy]= gradient(double(spIm));
    SegBG= ((Gx.^2+Gy.^2)~=0);
    figure;imshow(SegBG+currFrame(:,:,1));
    %}
    % matchOut= spTemplateMatch(currFrame,rainBuffer(:,:,:,[1,2,4,5]),spInfo,spMatchParams);
    referenceIdx= [1,2,4,5,3]; % curernt target frame positioned at the last
%     bufferIm= im2uint8(repmat(rainBuffer(:,:,1,referenceIdx(1:sizeBuffer-1)),1,1,3,1));
%     targetIm= im2uint8(repmat(currFrame(:,:,1),1,1,3));
    bufferIm= im2uint8(rainBufferY(:,:,referenceIdx(1:sizeBuffer-1)));
    targetIm= im2uint8(currFrameY);
   
    %% First round of matching, with SP mask and rain mask
    spMatchParams.useSpMask = 0;
    spMatchParams.useInputMask = 0;
    spMatchParams.f_splabel= int32(spInfo.spIm);
    spMatchParams.f_mask= uint8(ones(mRow,mCol));    
    currMatch = spMatching_mine(targetIm, bufferIm, spMatchParams);   
    for np= 1:spInfo.numSP     
        spMask= spInfo.spIm==(np-1); 
        spIdx= find(spMask);
        
        [rows,cols]= ind2sub([mRow,mCol],spIdx);       
        rowStart= min(rows);
        rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
        colStart= min(cols);
        colEnd= min(mCol,colStart+spMatchParams.blkSize-1); 
        % matching details for current SP
        spMatch= currMatch(np).matches;  
        spOffSet= currMatch(np).offsetToSpBox;
        T0= zeros(spMatchParams.blkSize,spMatchParams.blkSize,sizeBuffer);
        for nv=1:sizeBuffer
            currIdx= nv*10;
            r0= spMatch(3,currIdx)+spOffSet(1)+1;
            r1= min(mRow,r0+spMatchParams.blkSize-1);
            c0= spMatch(4,currIdx)+spOffSet(2)+1;
            c1= min(mCol,c0+spMatchParams.blkSize-1);           
            T0(1:(r1-r0+1),1:(c1-c0+1),nv)= rainBufferY(r0:r1,c0:c1,referenceIdx(nv));           
        end
        accMask= sum((repmat(T0(:,:,sizeBuffer),1,1,sizeBuffer)-T0)>0.005,3)>=3;        
        
        currRainMask(rowStart:rowEnd, colStart:colEnd)= ...
                accMask(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));    
        rainMask(spIdx)= currRainMask(spIdx); % figure;imshow(rainMask);
    end
    rainMask= rainMask.*(~currUV);
        
    %% second round of matching, with SP mask and rain mask
    spMatchParams.useSpMask = 1;
    spMatchParams.useInputMask = 1;
    spMatchParams.f_splabel= int32(spInfo.spIm);
    spMatchParams.f_mask= uint8(~rainMask);    
    currMatch2 = spMatching_mine(targetIm, bufferIm, spMatchParams);    
    for np= 1:spInfo.numSP     
        spMask= spInfo.spIm==(np-1);
        spIdx= find(spMask);
        
        [rows,cols]= ind2sub([mRow,mCol],spIdx);       
        rowStart= min(rows);
        rowEnd= min(mRow,rowStart+spMatchParams.blkSize-1);
        colStart= min(cols);
        colEnd= min(mCol,colStart+spMatchParams.blkSize-1); 
        % matching details for current SP
        spMatch= currMatch2(np).matches;  
        spOffSet= currMatch2(np).offsetToSpBox;                
        T1= zeros(spMatchParams.blkSize,spMatchParams.blkSize,numMatch);   
        [errorVal,errorIdx]= sort(spMatch(1,1:40),'ascend');
        % [~,errorIdx]= sort(spMatch(1,1:numMatch*(sizeBuffer-1)),'ascend');
        ne=0;
        for ni=1:numMatch
            if(errorVal(ni)<0.1)
                ne=ne+1;
            end
            currIdx= errorIdx(ni);
            nv= ceil(currIdx/10);
            r0= spMatch(3,currIdx)+spOffSet(1)+1;
            r1= min(mRow,r0+spMatchParams.blkSize-1);
            c0= spMatch(4,currIdx)+spOffSet(2)+1;
            c1= min(mCol,c0+spMatchParams.blkSize-1);                        
            T1(1:(r1-r0+1),1:(c1-c0+1),ni)= rainBufferY(r0:r1,c0:c1,referenceIdx(nv));       
        end
        if(ne<5)
           	currSPCompensate= mean(T1(:,:,1:5),3);
        else
            currSPCompensate= mean(T1,3);
        end
        currRainCompensateY(rowStart:rowEnd, colStart:colEnd)= ...
                currSPCompensate(1:(rowEnd-rowStart+1),1:(colEnd-colStart+1));    
        rainCompensateY(spIdx)= currRainCompensateY(spIdx); % figure;imshow(rainCompensateY);        
    end
    
    rainRemove(:,:,1)= currFrame(:,:,1).*(1-rainMask)+ rainCompensateY.*rainMask;
    rainRemove(:,:,2:3)= currFrame(:,:,2:3); % figure;imshow(rainRemove);

    rainRemove= ycbcr2rgb(rainRemove);
    
    imshow([ycbcr2rgb(currFrame),repmat(rainMask,1,1,3),rainRemove]);
    % imshow([ycbcr2rgb(currFrame),rainRemove]);

end
